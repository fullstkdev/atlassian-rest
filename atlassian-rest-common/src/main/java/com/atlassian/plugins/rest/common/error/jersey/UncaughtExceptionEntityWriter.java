package com.atlassian.plugins.rest.common.error.jersey;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

/**
 * Writes {@link UncaughtExceptionEntity}s out as plain text.
 */
@Provider
@Produces(MediaType.TEXT_PLAIN)
public class UncaughtExceptionEntityWriter implements MessageBodyWriter<UncaughtExceptionEntity> {
    public long getSize(UncaughtExceptionEntity t, Class<?> type, Type genericType, Annotation[] annotations,
                        MediaType mediaType) {
        return -1;
    }

    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return UncaughtExceptionEntity.class.isAssignableFrom(type);
    }

    public void writeTo(UncaughtExceptionEntity t, Class<?> type, Type genericType, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
            throws IOException, WebApplicationException {
        entityStream.write(t.getStackTrace().getBytes("utf-8"));
    }
}
