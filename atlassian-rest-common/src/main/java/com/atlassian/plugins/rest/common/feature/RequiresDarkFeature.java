package com.atlassian.plugins.rest.common.feature;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a REST resource class or method as requiring that the given dark feature(s) are enabled.
 * <p>
 * Allows multiple feature keys to be specified, in which case ALL are required to be enabled (e.g. flags are treated as an AND condition).
 * <p>
 * The annotation can be applied at the resource class and method level to allow fine-grained control of access.
 *
 * e.g.
 * <pre>
 *     &#064;RequiresDarkFeature("my.plugin.enabled")
 *     &#064;Path("/")
 *     class MyResource
 *     {
 *         ...
 *         &#064;GET
 *         &#064;Produces("application/json")
 *         &#064;RequiresDarkFeature("my.plugin.feature1")
 *         public Response getSomething()
 *         {
 *             ...
 *         }
 *     }
 * </pre>
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiresDarkFeature {
    /**
     * The list of dark feature keys that are required. Treated as an AND.
     */
    String[] value();
}
