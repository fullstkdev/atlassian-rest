package com.atlassian.plugins.rest.common.feature.jersey;

import com.atlassian.plugins.rest.common.feature.RequiresDarkFeature;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import javax.annotation.Nonnull;
import javax.ws.rs.ext.Provider;

import static com.atlassian.plugins.rest.common.util.ReflectionUtils.getAnnotation;
import static com.google.common.base.Preconditions.checkNotNull;

@Provider
public class DarkFeatureResourceFilterFactory implements ResourceFilterFactory {
    private static final Logger log = LoggerFactory.getLogger(DarkFeatureResourceFilterFactory.class);

    private final DarkFeatureManager darkFeatureManager;

    public DarkFeatureResourceFilterFactory(@Nonnull final DarkFeatureManager darkFeatureManager) {
        this.darkFeatureManager = checkNotNull(darkFeatureManager);
    }

    @Override
    public List<ResourceFilter> create(final AbstractMethod am) {
        if (getAnnotation(RequiresDarkFeature.class, am) != null ||
                getAnnotation(RequiresDarkFeature.class, am.getResource()) != null) {
            log.debug("RequiresDarkFeature annotation found - creating filter");
            return Collections.<ResourceFilter>singletonList(new DarkFeatureResourceFilter(am, darkFeatureManager));
        }
        log.debug("No RequiresDarkFeature annotation found - not creating filter");
        return Collections.emptyList();
    }
}
