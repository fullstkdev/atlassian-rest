package com.atlassian.plugins.rest.common.json;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.map.Module;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.SerializationConfig;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;

import static com.google.common.collect.ImmutableList.copyOf;

public class DefaultJaxbJsonMarshaller implements JaxbJsonMarshaller {

    private final boolean prettyPrint;
    private final JacksonJsonProvider jsonProvider;

    /**
     * @deprecated since 2.9.2. Use {@link #builder()}
     */
    @Deprecated
    public DefaultJaxbJsonMarshaller() {
        this(null, false);
    }

    /**
     * @deprecated since 2.9.2. Use {@link #builder()}
     */
    @Deprecated
    public DefaultJaxbJsonMarshaller(boolean prettyPrint) {
        this(null, prettyPrint);
    }

    private DefaultJaxbJsonMarshaller(Iterable<? extends Module> modules, boolean prettyPrint) {
        this.prettyPrint = prettyPrint;

        // Don't use JsonGenerator directly as we want to make sure we use the same
        // configuration as for REST requests in OsgiResourceConfig.
        Iterable<? extends Module> moduleList = modules != null ? copyOf(modules) : Collections.<Module>emptyList();
        this.jsonProvider = new JacksonJsonProviderFactory().create(moduleList);
    }

    public String marshal(Object jaxbBean) {
        try {
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            if (prettyPrint) {
                jsonProvider.enable(SerializationConfig.Feature.INDENT_OUTPUT, true);
            }
            jsonProvider.writeTo(jaxbBean, jaxbBean.getClass(), null, null, MediaType.APPLICATION_JSON_TYPE, null, os);

            // The encoding used inside JacksonJsonProvider is always UTF-8
            return new String(os.toByteArray(), JsonEncoding.UTF8.getJavaName());
        } catch (IOException e) {
            throw new JsonMarshallingException(e);
        }
    }

    @Deprecated
    public String marshal(final Object jaxbBean, final Class... jaxbClasses) throws JAXBException {
        return marshal(jaxbBean);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private boolean prettyPrint;
        private Iterable<? extends Module> modules;

        private Builder() {
        }

        public Builder prettyPrint(boolean prettyPrint) {
            this.prettyPrint = prettyPrint;
            return this;
        }

        public Builder modules(Iterable<? extends Module> modules) {
            this.modules = modules;
            return this;
        }

        public JaxbJsonMarshaller build() {
            return new DefaultJaxbJsonMarshaller(modules, prettyPrint);
        }
    }
}
