package com.atlassian.plugins.rest.common.json;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.BeanDescription;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.Module;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.Serializers;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.ser.BeanSerializerFactory;
import org.codehaus.jackson.map.type.TypeBindings;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.map.type.TypeModifier;
import org.codehaus.jackson.type.JavaType;

import java.lang.reflect.Type;

class GuavaIterableCapableModule extends Module {
    // TODO: REST-337 remove this and just use FluentIterable when we past guava-11 compatibility problems
    static private final Class fluentIterableClass;

    static {
        Class tmp = null;
        try {
            tmp = Class.forName("com.google.common.collect.FluentIterable");
        } catch (ClassNotFoundException e) {
            // that's OK - we don't have FluentIterable
        }
        fluentIterableClass = tmp;
    }

    static private final TypeModifier guavaIterableTypeModifier = new TypeModifier() {
        @Override
        public JavaType modifyType(final JavaType type, final Type jdkType, final TypeBindings context, final TypeFactory typeFactory) {
            final Class<?> raw = type.getRawClass();
            if (fluentIterableClass.isAssignableFrom(raw)) {
                return typeFactory.constructParametricType(Iterable.class, typeFactory.unknownType());
            }
            return type;
        }
    };

    static private final Serializers.Base guavaSerializers = new Serializers.Base() {
        @Override
        public JsonSerializer<?> findSerializer(final SerializationConfig config, final JavaType type, final BeanDescription beanDesc, final BeanProperty property) {
            final Class<?> raw = type.getRawClass();
            if (fluentIterableClass.isAssignableFrom(raw)) {
                BasicBeanDescription basicBeanDescription = config.introspect(type);
                try {
                    return BeanSerializerFactory.instance.findSerializerByAddonType(config, type, basicBeanDescription, property, false);
                } catch (JsonMappingException e) {
                    // Had no luck - continue with super method
                }
            }
            return super.findSerializer(config, type, beanDesc, property);
        }
    };

    private final static Version version = new Version(0, 1, 0, null);

    @Override
    public String getModuleName() {
        return GuavaIterableCapableModule.class.getSimpleName();
    }

    @Override
    public Version version() {
        return version;
    }

    @Override
    public void setupModule(final SetupContext context) {
        if (fluentIterableClass != null) {
            context.addTypeModifier(guavaIterableTypeModifier);
            context.addSerializers(guavaSerializers);
        }
    }
}
