package com.atlassian.plugins.rest.common.multipart.fileupload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import com.atlassian.plugins.rest.common.multipart.FilePart;
import com.atlassian.plugins.rest.common.multipart.UnsupportedFileNameEncodingException;

import com.google.common.base.Preconditions;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.util.mime.MimeUtility;

public final class CommonsFileUploadFilePart implements FilePart {
    private final FileItem fileItem;
    private final String name;

    CommonsFileUploadFilePart(FileItem fileItem) {
        this.fileItem = Preconditions.checkNotNull(fileItem);
        try {
            if (fileItem.getName() == null) {
                name = null;
            } else {
                this.name = new File(
                    MimeUtility.decodeText(fileItem.getName())
                ).getName();
            }

        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedFileNameEncodingException(fileItem.getName());
        }
    }

    public String getName() {
        return name;
    }

    public InputStream getInputStream() throws IOException {
        return fileItem.getInputStream();
    }

    public String getContentType() {
        return fileItem.getContentType();
    }

    public void write(final File file) throws IOException {
        try {
            fileItem.write(file);
        } catch (Exception e) {
            if (e instanceof IOException) {
                throw (IOException) e;
            } else {
                throw new IOException(e);
            }
        }
    }

    public String getValue() {
        return fileItem.getString();
    }

    public boolean isFormField() {
        return fileItem.isFormField();
    }
}