package com.atlassian.plugins.rest.common.security.jersey;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

import javax.ws.rs.ext.Provider;

/**
 * This class adds a 'X-Content-Type-Options' header to responses to
 * prevent certain browsers from performing mime-type sniffing.
 *
 * See http://blogs.msdn.com/b/ie/archive/2008/07/02/ie8-security-part-v-comprehensive-protection.aspx
 * for further information.
 *
 * @since 2.8.1
 */

@Provider
public class AntiSniffingResponseFilter implements ContainerResponseFilter {
    public static final String ANTI_SNIFFING_HEADER_NAME = "X-Content-Type-Options";
    public static final String ANTI_SNIFFING_HEADER_VALUE = "nosniff";

    public ContainerResponse filter(ContainerRequest request,
                                    ContainerResponse containerResponse) {
        containerResponse.getHttpHeaders().add(
                ANTI_SNIFFING_HEADER_NAME, ANTI_SNIFFING_HEADER_VALUE);
        return containerResponse;
    }
}
