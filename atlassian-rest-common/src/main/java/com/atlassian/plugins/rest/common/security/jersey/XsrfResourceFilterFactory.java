package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaultsModuleDescriptor;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.core.Response;
import javax.ws.rs.POST;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.Collections;
import java.util.List;

/**
 * Factory for the XSRF resource filter
 *
 * @since 2.4
 */
@Provider
public class XsrfResourceFilterFactory implements ResourceFilterFactory {
    private static final Logger log = LoggerFactory.getLogger(
            XsrfResourceFilterFactory.class);
    private static final String LEGACY_FEATURE_KEY = "atlassian.rest.xsrf.legacy.enabled";

    private final HttpContext httpContext;
    private final XsrfRequestValidator xsrfRequestValidator;
    private final PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> pluginModuleTracker;
    private final DarkFeatureManager darkFeatureManager;

    public XsrfResourceFilterFactory(
            HttpContext httpContext,
            XsrfRequestValidator xsrfRequestValidator,
            PluginAccessor pluginAccessor,
            PluginEventManager pluginEventManager,
            DarkFeatureManager darkFeatureManager) {
        this.httpContext = Preconditions.checkNotNull(httpContext);
        this.xsrfRequestValidator = Preconditions.checkNotNull(xsrfRequestValidator);
        this.pluginModuleTracker = new DefaultPluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor>(
                pluginAccessor, pluginEventManager, CorsDefaultsModuleDescriptor.class);
        this.darkFeatureManager = darkFeatureManager;
    }

    @VisibleForTesting
    boolean hasRequiresXsrfCheckAnnotation(AnnotatedElement annotatedElement) {
        if (annotatedElement.isAnnotationPresent(RequiresXsrfCheck.class)) {
            return true;
        }
        for (Annotation annotation : annotatedElement.getAnnotations()) {
            if (annotation.annotationType().getSimpleName().equals(
                    RequiresXsrfCheck.class.getSimpleName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if legacy XSRF mode is active, otherwise returns false.
     *
     * @since 3.0.0
     * @deprecated Only provided for legacy reasons, will be removed in 4.0.0
     */
    @VisibleForTesting
    @Deprecated
    boolean inLegacyXsrfMode() {
        return darkFeatureManager.isFeatureEnabledForAllUsers(LEGACY_FEATURE_KEY);
    }

    /**
     * Returns true if any of the given annotations is the
     * XsrfProtectionExcluded annotation.
     *
     * @param annotations the annotations to check against.
     * @return true if any of the given annotations is the
     * XsrfProtectionExcluded annotation. Otherwise returns false.
     */
    private static boolean isXsrfProtectionExcludedAnnotationPresent(
            Annotation[] annotations) {
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().getCanonicalName().equals(
                    XsrfProtectionExcluded.class.getCanonicalName())) {
                if (!annotation.annotationType().equals(
                        XsrfProtectionExcluded.class)) {
                    log.warn("Detected usage of the " +
                            "com.atlassian.annotations.security." +
                            "XsrfProtectionExcluded annotation loaded " +
                            "from elsewhere. " +
                            XsrfProtectionExcluded.class.getClassLoader() +
                            " != " + annotation.annotationType().getClassLoader()
                    );
                }
                return true;
            }
        }
        return false;
    }

    public List<ResourceFilter> create(final AbstractMethod method) {
        final boolean hasEnforceAnnotation = hasRequiresXsrfCheckAnnotation(
            method) || hasRequiresXsrfCheckAnnotation(method.getResource());
        final boolean hasExcludeAnnotation =
            isXsrfProtectionExcludedAnnotationPresent(method.getAnnotations());
        XsrfResourceFilter xsrfResourceFilter = null;
        if (inLegacyXsrfMode()) {
            if (!hasExcludeAnnotation) {
                if (hasEnforceAnnotation) {
                    xsrfResourceFilter = new XsrfResourceFilter();
                } else if (method.getAnnotations().length != 0) {
                    xsrfResourceFilter = new OriginBasedXsrfResourceFilter();
                }
                if (xsrfResourceFilter != null) {
                    xsrfResourceFilter.setFailureStatus(Response.Status.NOT_FOUND);
                }
            }
        } else if ((
            method.isAnnotationPresent(GET.class) && hasEnforceAnnotation) ||
            (!method.isAnnotationPresent(GET.class) && !hasExcludeAnnotation)) {
            xsrfResourceFilter = new XsrfResourceFilter();
        }
        if (xsrfResourceFilter != null) {
            xsrfResourceFilter.setHttpContext(httpContext);
            xsrfResourceFilter.setXsrfRequestValidator(xsrfRequestValidator);
            xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker);
            return Collections.<ResourceFilter>singletonList(xsrfResourceFilter);
        }
        return Collections.emptyList();
    }
}
