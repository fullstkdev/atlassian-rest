package com.atlassian.plugins.rest.common.util;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;

/**
 * A class to simplify some reflection calls.
 */
public class ReflectionUtils {
    private static final Logger log = LoggerFactory.getLogger(ReflectionUtils.class);

    private ReflectionUtils() {
    }

    /**
     * Gets the value of the {@link Field field} for the given object. It will change the accessibility of the field if necessary.
     * Setting it back to its original value at the end of the method call.
     *
     * @param field  the field to read from
     * @param object the object to read the field from
     * @return the value of the field.
     */
    public static Object getFieldValue(Field field, Object object) {
        final boolean accessible = field.isAccessible();
        try {
            if (!accessible) {
                field.setAccessible(true);
            }
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Could not access '" + field + "' from '" + object + "'", e);
        } finally {
            if (!accessible) {
                field.setAccessible(false);
            }
        }
    }

    /**
     * Sets the value of the {@link Field field} for the given object. It will change the accessibility of the field if necessary.
     * Setting it back to its original value at the end of the method call.
     *
     * @param field  the field to set the value of
     * @param object the object to for which to set the field value.
     * @param value  the new value to be set to the field of object.
     */
    public static void setFieldValue(Field field, Object object, Object value) {
        final boolean accessible = field.isAccessible();
        try {
            if (!accessible) {
                field.setAccessible(true);
            }
            field.set(object, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Could not access '" + field + "' from '" + object + "'", e);
        } finally {
            if (!accessible) {
                field.setAccessible(false);
            }
        }
    }

    /**
     * Returns the result of running {@link Class#getDeclaredFields()} on the
     * supplied class, as well as all its super types. Fields are ordered in
     * ascending hierarchy order (subclasses first).
     *
     * @param clazz
     * @return all of the class's fields (including inherited fields).
     * @since v1.0.4
     */
    public static List<Field> getDeclaredFields(Class clazz) {
        if (clazz == null) {
            return Lists.newArrayList();
        } else {
            final List<Field> superFields = getDeclaredFields(clazz.getSuperclass());
            superFields.addAll(0, asList(clazz.getDeclaredFields()));
            return superFields;
        }
    }

    /**
     * Gets the provided annotation from the provided annotated element, if it exists.
     * <p>
     * Will only return a result if the found annotation is from the same class loader as the target type.
     * <p>
     * Will emit a warning into the log if the annotation is found but has been loaded
     * from a different class loader.
     *
     * @param annotationType The annotation to search for
     * @param element        The element to search
     * @return the element's annotation for the specified annotation type if present, else <code>null</code>
     * @since 3.0.10
     */
    public static <T extends Annotation> T getAnnotation(@Nonnull final Class<T> annotationType, @Nullable final AnnotatedElement element) {
        checkNotNull(annotationType, "An annotation is required");

        if (element == null) {
            return null;
        }

        for (Annotation a : element.getAnnotations()) {
            if (StringUtils.equals(a.annotationType().getCanonicalName(), annotationType.getCanonicalName())) {
                if (!a.annotationType().equals(annotationType)) {
                    log.warn("Detected usage of the {} annotation loaded from elsewhere. {} != {}",
                            annotationType.getCanonicalName(),
                            annotationType.getClassLoader(),
                            a.annotationType().getClassLoader());
                    return null;
                }
                return (T) a;
            }
        }
        return null;
    }
}
