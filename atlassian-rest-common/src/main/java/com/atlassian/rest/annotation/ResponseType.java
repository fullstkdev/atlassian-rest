package com.atlassian.rest.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares an actual type returned by a REST method so that the WADL generator can create JSON schemas.
 *
 * <p>
 *   This annotation can be placed on a single method and also on a class. When it's on
 *   a class that means it's applied to all methods in this class. That's useful for declaring
 *   error types, as they are usually the same for all methods.
 * </p>
 *
 * <p>
 *   Use {@link ResponseTypes} annotation to declare more than one return type.
 * </p>
 *
 */
@Retention (RetentionPolicy.RUNTIME)
@Target ({ElementType.METHOD, ElementType.TYPE})
public @interface ResponseType
{
    enum StatusType
    {
        /**
         * All success (2xx) status codes
         */
        SUCCESS("2\\d\\d"),
        /**
         * All redirect (3xx) status codes
         */
        REDIRECTION("3\\d\\d"),
        /**
         * All client error (4xx) status codes
         */
        CLIENT_ERROR("4\\d\\d"),
        /**
         * All server error (5xx) status codes
         */
        SERVER_ERROR("5\\d\\d");

        private final String pattern;

        StatusType(String pattern)
        {
            this.pattern = pattern;
        }

        public boolean matches(int status)
        {
            return String.valueOf(status).matches(pattern);
        }
    }

    /**
     * Status for which this type applies. Note that for the JSON schema
     * generation to work, the type also needs to be declared in the javadoc.
     *
     * @return status
     */
    int status() default 0;

    /**
     * General type of the status.
     *
     * <p>
     *     Instead of specifying an exact status, you can select a group of statues.
     *     If any status from the group is present in the method's javadoc, it will
     *     have a schema generated for the type declared here.
     * </p>
     *
     * @return
     */
    StatusType statusType() default StatusType.SUCCESS;

    /**
     * Type of the returned response.
     */
    Class<?> value();

    /**
     * If the response class is generic you are supposed to provide all its generic types here.
     */
    Class<?>[] genericTypes() default {};
}