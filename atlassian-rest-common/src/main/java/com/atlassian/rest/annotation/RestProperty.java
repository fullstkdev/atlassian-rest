package com.atlassian.rest.annotation;

import com.google.common.collect.ImmutableSet;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Set;

/**
 * This annotation can give hints to the JSON schema generator about a bean property.
 */
@Retention (RetentionPolicy.RUNTIME)
@Target ({ ElementType.FIELD, ElementType.METHOD })
public @interface RestProperty
{
    public static enum Scope
    {
        /**
         * for fields available only in requests
         */
        REQUEST,
        /**
         * for fields available only in responses
         */
        RESPONSE,
        /**
         * for fields available in requests and in responses
         */
        BOTH,
        /**
         * if the field name is {@code self} or {@code expand} then RESPONSE, otherwise BOTH
         */
        AUTO;

        private final static Set<String> responseScopeByDefault = ImmutableSet.of("self", "expand");

        public boolean contains(Scope scope)
        {
            return this == BOTH || this == AUTO || this == scope;
        }

        public boolean includes(Scope fieldScope, String fieldName)
        {
            fieldScope = resolveAuto(fieldScope, fieldName);
            return fieldScope == BOTH || fieldScope == this || this == BOTH || this == AUTO;
        }

        private static Scope resolveAuto(Scope scope, String fieldName)
        {
            if (scope == AUTO)
            {
                return responseScopeByDefault.contains(fieldName) ? RESPONSE : BOTH;
            }
            else
            {
                return scope;
            }
        }
    }

    /**
     * Sets the scope for the field.
     *
     * <p>
     *  Each schema is generated in one of two scopes: REQUEST and RESPONSE.
     *  Use this annotation to control whether the field should appear in a scope.
     * </p>
     *
     */
    Scope scope() default Scope.AUTO;

    /**
     * Property pattern for {@code Map} fields.
     */
    String pattern() default ".+";

    /**
     * Description of the property.
     */
    String description() default "";

    /**
     * Whether the property is required.
     */
    boolean required() default false;
}