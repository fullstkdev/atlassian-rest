package com.atlassian.plugins.rest.common.expand;

import java.lang.annotation.Annotation;
import java.util.Collections;

import com.atlassian.plugins.rest.common.expand.parameter.DefaultExpandParameter;
import com.atlassian.plugins.rest.common.expand.parameter.ExpandParameter;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Testing {@link DefaultExpandParameter}
 */
public class DefaultExpandParameterTest {
    @Test
    public void testExpandParameterWithNull() {
        final ExpandParameter parameter = new DefaultExpandParameter(null);
        assertThat(parameter, emptyParameter());
    }

    @Test
    public void testExpandParameterWithEmptyString() {
        final ExpandParameter parameter = new DefaultExpandParameter(Collections.singleton(""));
        assertThat(parameter, emptyParameter());
    }

    @Test
    public void testExpandParameterWithValidString1() {
        final String parameterValue = "value";
        final ExpandParameter parameter = new DefaultExpandParameter(Collections.singleton(parameterValue));

        assertThat(parameter, not(emptyParameter()));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(parameterValue)), emptyParameter());
        assertThat(parameter, shouldExpand(parameterValue));
        assertThat(parameter, shouldNotExpand("shouldnot"));
    }

    @Test
    public void testExpandParameterWithValidString2() {
        final String value1 = "value1";
        final String value2 = "value2";

        final ExpandParameter parameter = new DefaultExpandParameter(Collections.singleton(value1 + "," + value2));

        assertThat(parameter, not(emptyParameter()));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value1)), emptyParameter());
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value2)), emptyParameter());
        assertThat(parameter, shouldExpand(value1));
        assertThat(parameter, shouldExpand(value2));
        assertThat(parameter, shouldNotExpand("shouldnot"));
    }

    @Test
    public void testExpandParameterWithValidString3() {
        final String value1 = "value1";
        final String value2 = "value2";

        final ExpandParameter parameter = new DefaultExpandParameter(Collections.singleton(value1 + "." + value2));

        assertThat(parameter, not(emptyParameter()));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value1)), not(emptyParameter()));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value2)), emptyParameter());
        assertThat(parameter, shouldExpand(value1));
        assertThat(parameter, shouldNotExpand(value2));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value1)), shouldExpand(value2));
        assertThat(parameter, shouldNotExpand("shouldnot"));
    }

    @Test
    public void testExpandParameterWithWilcard1() {
        final String value1 = "value1";
        final String value2 = "value2";

        final ExpandParameter parameter = new DefaultExpandParameter(Collections.singleton("*" + "." + value2));

        assertThat(parameter, not(emptyParameter()));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value1)), not(emptyParameter()));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value2)), not(emptyParameter()));
        assertThat(parameter, shouldExpand(value1));
        assertThat(parameter, shouldExpand(value2));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value1)), shouldExpand(value2));
        assertThat(parameter, shouldExpand("should"));
    }

    @Test
    public void testExpandParameterWithWilcard2() {
        final String value1 = "value1";
        final String value2 = "value2";

        final ExpandParameter parameter = new DefaultExpandParameter(Collections.singleton(value1 + "." + "*"));

        assertThat(parameter, not(emptyParameter()));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value1)), not(emptyParameter()));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value2)), emptyParameter());
        assertThat(parameter, shouldExpand(value1));
        assertThat(parameter, shouldNotExpand(value2));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value1)), shouldExpand(value2));
        assertThat(parameter, shouldNotExpand("shouldnot"));
        assertThat(parameter.getExpandParameter(getExpandableAnnotation(value1)), shouldExpand("should"));
    }

    @Test
    public void testExpandParameterWithMultipleSubitemsSamePrefix() {
        final ExpandParameter parameter = new DefaultExpandParameter(Collections.singleton("root.child1,root.child2,root.child2.grandchild"));

        assertThat(parameter, not(emptyParameter()));
        assertThat(parameter.shouldExpand(getExpandableAnnotation("root")), is(true));

        ExpandParameter root = parameter.getExpandParameter(getExpandableAnnotation("root"));
        assertThat(root, not(emptyParameter()));

        assertThat(root, shouldExpand("child1"));
        assertThat(root, shouldExpand("child2"));

        ExpandParameter child2 = root.getExpandParameter(getExpandableAnnotation("child2"));
        assertThat(child2, shouldExpand("grandchild"));
    }

    private Expandable getExpandableAnnotation(final String parameterValue) {
        return new Expandable() {
            public String value() {
                return parameterValue;
            }

            public Class<? extends Annotation> annotationType() {
                return Expandable.class;
            }
        };
    }

    private Matcher<ExpandParameter> emptyParameter() {
        return new FeatureMatcher<ExpandParameter, Boolean>(is(true), "an ExpandParameter for which isEmpty", "isEmpty") {
            @Override
            protected Boolean featureValueOf(final ExpandParameter expandParameter) {
                return expandParameter.isEmpty();
            }
        };
    }

    private Matcher<ExpandParameter> shouldExpand(final String parameterValue) {
        return new FeatureMatcher<ExpandParameter, Boolean>(is(true), "ExpandParameter for which shouldExpand", "shouldExpand") {
            @Override
            protected Boolean featureValueOf(final ExpandParameter expandParameter) {
                return expandParameter.shouldExpand(getExpandableAnnotation(parameterValue));
            }
        };
    }

    private Matcher<ExpandParameter> shouldNotExpand(String parameterValue) {
        return not(shouldExpand(parameterValue));
    }
}
