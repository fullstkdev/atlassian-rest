package com.atlassian.plugins.rest.common.feature.jersey;

import com.sun.jersey.api.NotFoundException;
import org.junit.Test;
import org.mockito.InjectMocks;

import static com.atlassian.plugins.rest.common.feature.jersey.test.DarkFeatureTestResource.FEATURE_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DarkFeatureResourceFilterTest extends AbstractDarkFeatureResourceFilterTest {
    @InjectMocks
    private DarkFeatureResourceFilter classUnderTest;

    @Test
    public void testFilterWithNoAnnotations() {
        setResourceAnnotated(false);
        setMethodAnnotated(false);

        assertThat(classUnderTest.filter(request), equalTo(request));
    }

    @Test
    public void testFilterWithResourceAnnotationAndFeatureEnabled() {
        setFeatureEnabled(true);
        setResourceAnnotated(true);
        setMethodAnnotated(false);
        setResourceAnnotation(FEATURE_KEY);

        assertThat(classUnderTest.filter(request), equalTo(request));
    }

    @Test(expected = NotFoundException.class)
    public void testFilterWithResourceAnnotationAndFeatureDisabled() {
        setFeatureEnabled(false);
        setResourceAnnotated(true);
        setMethodAnnotated(false);
        setResourceAnnotation(FEATURE_KEY);

        classUnderTest.filter(request);
    }

    @Test
    public void testFilterWithMethodAnnotationAndFeatureEnabled() {
        setFeatureEnabled(true);
        setResourceAnnotated(false);
        setMethodAnnotated(true);

        assertThat(classUnderTest.filter(request), equalTo(request));
    }

    @Test(expected = NotFoundException.class)
    public void testFilterWithMethodAnnotationAndFeatureDisabled() {
        setFeatureEnabled(false);
        setResourceAnnotated(false);
        setMethodAnnotated(true);

        classUnderTest.filter(request);
    }

    @Test
    public void testFilterWithMultipleKeysRequiredAndAllEnabled() {
        setResourceAnnotated(true);
        setResourceAnnotation("feature1", "feature2");
        setFeatureEnabled("feature1", true);
        setFeatureEnabled("feature2", true);

        assertThat(classUnderTest.filter(request), equalTo(request));
    }

    @Test(expected = NotFoundException.class)
    public void testFilterWithMultipleKeysRequiredAndNotAllEnabled() {
        setResourceAnnotated(true);
        setResourceAnnotation("feature1", "feature2");
        setFeatureEnabled("feature1", true);
        setFeatureEnabled("feature2", false);

        classUnderTest.filter(request);
    }
}