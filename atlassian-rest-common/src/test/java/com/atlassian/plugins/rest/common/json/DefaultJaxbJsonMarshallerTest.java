package com.atlassian.plugins.rest.common.json;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.ser.std.IterableSerializer;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static org.apache.commons.lang.SystemUtils.LINE_SEPARATOR;
import static org.junit.Assert.assertEquals;

public class DefaultJaxbJsonMarshallerTest {
    private JaxbJsonMarshaller marshaller;

    @Before
    public void setUp() {
        marshaller = DefaultJaxbJsonMarshaller.builder().build();
    }

    @Test
    public void testMarshalBoolean() throws JAXBException {
        assertEquals("true", marshaller.marshal(Boolean.TRUE));
    }

    @Test
    public void testMarshalUrlWithSpaces() throws Exception {
        assertEquals("\"https://user:paswd@a.b.c:8443/path/to%20file/on%20disk/aaa%20bbb%20ccc.p%20d%20f?q%20u%20e%20r%20y#frag%20ment\"",
                marshaller.marshal(new URI("https", "user:paswd", "a.b.c", 8443, "/path/to file/on disk/aaa bbb ccc.p d f", "q u e r y", "frag ment")));
    }

    @Test
    public void testMarshalInteger() throws JAXBException {
        assertEquals("2", marshaller.marshal(Integer.valueOf(2)));
    }

    @Test
    public void testMarshalGuavaIterable() throws JAXBException {
        assertEquals("[\"ad\",\"bd\",\"cd\"]", marshaller.marshal(
                Iterables.transform(Arrays.asList(new String[]{"a", "b", "c"}), new Function<String, String>() {
                    @Nullable
                    @Override
                    public String apply(final String input) {
                        return input + "d";
                    }
                })
        ));
    }

    @Test
    public void testMarshalEmptyGuavaIterable() throws JAXBException {
        assertEquals("[]", marshaller.marshal(Iterables.transform(Lists.<String>newLinkedList(), new Function<String, String>() {
                    @Nullable
                    @Override
                    public String apply(final String input) {
                        return input + "d";
                    }
                })
        ));
    }

    static class JsonBeanWithIterable {
        private final Iterable<String> strings;

        public JsonBeanWithIterable(Iterable<String> strings) {
            this.strings = strings;
        }

        @JsonProperty
        public Iterable<String> getStrings() {
            return strings;
        }
    }

    @Test
    public void testMarshalEmptyGuavaIterableInObject() throws JAXBException {
        assertEquals("{\"strings\":[]}", marshaller.marshal(new JsonBeanWithIterable(Iterables.transform(Lists.<String>newLinkedList(), new Function<String, String>() {
            @Nullable
            @Override
            public String apply(final String input) {
                return input + "d";
            }
        })
        )));
    }

    @Test
    public void testMarshalNotEmptyGuavaIterableInObject() throws JAXBException {
        assertEquals("{\"strings\":[\"testd\"]}", marshaller.marshal(new JsonBeanWithIterable(Iterables.transform(Arrays.asList(new String[]{"test"}), new Function<String, String>() {
            @Nullable
            @Override
            public String apply(final String input) {
                return input + "d";
            }
        })
        )));
    }

    static class IterableBean implements Iterable<String> {
        @JsonProperty
        private long id;

        @JsonProperty
        private List<String> tabs;

        public IterableBean(final long id, final Iterable<String> tabs) {
            this.id = id;
            this.tabs = copyOf(tabs);
        }

        public long getId() {
            return id;
        }

        public List<String> getTabs() {
            return tabs;
        }

        @Override
        public Iterator<String> iterator() {
            return getTabs().iterator();
        }
    }

    @Test
    public void testMarshalIterableBean() {
        assertEquals("{\"id\":1,\"tabs\":[\"tab1\",\"tab2\"]}", marshaller.marshal(
                new IterableBean(1, Lists.newArrayList("tab1", "tab2"))));
    }

    @Test
    public void testMarshalString() throws JAXBException {
        assertEquals("\"foobar\"", marshaller.marshal("foobar"));
    }

    @Test
    public void testMarshalMap() throws JAXBException {
        assertEquals("{\"foo\":\"bar\"}", marshaller.marshal(Collections.singletonMap("foo", "bar")));
    }

    @Test
    public void testMarshalList() throws JAXBException {
        assertEquals("[\"foo\",\"bar\"]", marshaller.marshal(Arrays.asList("foo", "bar")));
    }

    @Test
    public void testMarshalObjectWithMember() throws Exception {
        assertEquals("{\"string\":\"foo\"}", marshaller.marshal(new ObjectWithMember("foo")));
    }

    @Test
    public void testMarshalObjectWithNullMember() throws Exception {
        assertEquals("{}", marshaller.marshal(new ObjectWithMember(null)));
    }

    @Test
    public void testMarshalObjectWithMemberMissingAnnotation() throws Exception {
        assertEquals("{}", marshaller.marshal(new ObjectWithMemberMissingAnnotation("foo")));
    }

    @Test
    public void testMarshalObjectWithMemberWithRenaming() throws Exception {
        assertEquals("{\"str\":\"foo\"}", marshaller.marshal(new ObjectWithMemberWithRenaming("foo")));
    }

    @Test
    public void marshalCanPrettyPrint() throws Exception {
        marshaller = DefaultJaxbJsonMarshaller.builder().prettyPrint(true).build();
        assertEquals("{" + LINE_SEPARATOR + "  \"str\" : \"foo\"" + LINE_SEPARATOR + "}", marshaller.marshal(new ObjectWithMemberWithRenaming("foo")));
    }

    @Test
    public void testMarshalObjectWithJsonAnnotatedPropertyNull() throws Exception {
        assertEquals("Default behaviour with @JsonProperty annotations should exclude null members",
                "{}", marshaller.marshal(new JsonBeanInclusionDefault(null)));
    }

    @Test
    public void testMarshalObjectWithJsonSerializeAnnotationButNoExplicitInclusionMeansAlways() throws Exception {
        assertEquals("Behaviour with @JsonSerialize includes non-null members",
                "{\"name\":null}", marshaller.marshal(new JsonBeanWithAnnotationButNoExplicitInclusion(null)));
    }

    @Test
    public void testMarshalObjectWithJsonAnnotatedPropertyNullAndAlwaysInclusion() throws Exception {
        assertEquals("If we ask for null members to always be included they should be in the result",
                "{\"name\":null}", marshaller.marshal(new JsonBeanInclusionAlways(null)));
    }

    @Test
    public void testMarshalObjectWithJsonAnnotatedPropertyNullAndNonNullInclusion() throws Exception {
        assertEquals("Non-NULL inclusion means null members should not be in the output",
                "{}", marshaller.marshal(new JsonBeanInclusionNonNull(null)));
    }

    @XmlRootElement
    private static class ObjectWithMember {
        @SuppressWarnings("unused")
        @XmlElement
        private final String string;

        public ObjectWithMember(String string) {
            this.string = string;
        }
    }

    @XmlRootElement
    private static class ObjectWithMemberMissingAnnotation {
        @SuppressWarnings("unused")
        private final String string;

        public ObjectWithMemberMissingAnnotation(String string) {
            this.string = string;
        }
    }

    @XmlRootElement
    private static class ObjectWithMemberWithRenaming {
        @SuppressWarnings("unused")
        @XmlElement(name = "str")
        private final String string;

        public ObjectWithMemberWithRenaming(String string) {
            this.string = string;
        }
    }

    static class JsonBeanInclusionDefault {
        @JsonProperty
        public final String name;

        JsonBeanInclusionDefault(String name) {
            this.name = name;
        }
    }

    @JsonSerialize
    static class JsonBeanWithAnnotationButNoExplicitInclusion {
        @JsonProperty
        public final String name;

        JsonBeanWithAnnotationButNoExplicitInclusion(String name) {
            this.name = name;
        }
    }

    @JsonSerialize(include = Inclusion.ALWAYS)
    static class JsonBeanInclusionAlways {
        @JsonProperty
        public final String name;

        JsonBeanInclusionAlways(String name) {
            this.name = name;
        }
    }

    @JsonSerialize(include = Inclusion.NON_NULL)
    static class JsonBeanInclusionNonNull {
        @JsonProperty
        public final String name;

        JsonBeanInclusionNonNull(String name) {
            this.name = name;
        }
    }
}

