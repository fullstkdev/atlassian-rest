package com.atlassian.plugins.rest.common.multipart.fileupload;

import com.atlassian.plugins.rest.common.multipart.UnsupportedFileNameEncodingException;

import org.apache.commons.fileupload.FileItem;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CommonsFileUploadFilePartTest {
    private FileItem fileItem;

    @Before
    public void setUp() {
        fileItem = mock(FileItem.class);
    }

    @Test
    public void fileItemWithNullFileNameShouldSimplyReturnNull() {
        CommonsFileUploadFilePart filePart = new CommonsFileUploadFilePart(fileItem);
        assertNull(filePart.getName());
    }

    @Test
    public void fileItemWithASCIIFileNameShouldReturnASCIIValue() {
        when(fileItem.getName()).thenReturn("normalchars");
        CommonsFileUploadFilePart filePart = new CommonsFileUploadFilePart(fileItem);
        assertEquals("normalchars", filePart.getName());
    }

    @Test
    public void fileItemWithEncodedUTF8FileNameShouldReturnUTF8Value() {
        when(fileItem.getName()).thenReturn("=?utf-8?B?0YLQtdGB0YI=?=");
        CommonsFileUploadFilePart filePart = new CommonsFileUploadFilePart(fileItem);
        assertEquals("тест", filePart.getName());
    }

    @Test
    public void fileItemWithUnencodedUTF8FileNameShouldReturnUTF8Value() {
        when(fileItem.getName()).thenReturn("тест");
        CommonsFileUploadFilePart filePart = new CommonsFileUploadFilePart(fileItem);
        assertEquals("тест", filePart.getName());
    }

    @Test(expected = UnsupportedFileNameEncodingException.class)
    public void fileItemNamedWithUnsupportedCharacterEncodingShouldThrowException() {
        when(fileItem.getName()).thenReturn("=?utf-unknown?B?unknown?=");
        new CommonsFileUploadFilePart(fileItem);
    }

    @Test
    public void testGetName() {
        CommonsFileUploadFilePart filePart;
        for (String fileName : Arrays.asList("/tmp/file", "what", "../../what")) {
            final String expectedName = new File(fileName).getName();
            FileItem fileItem = getMockFileItem(fileName);
            filePart = new CommonsFileUploadFilePart(fileItem);
            assertThat(filePart.getName(), is(expectedName));
        }
    }

    private FileItem getMockFileItem(String filename) {
        FileItem fileItem = mock(FileItem.class);
        when(fileItem.getName()).thenReturn(filename);
        return fileItem;
    }

}
