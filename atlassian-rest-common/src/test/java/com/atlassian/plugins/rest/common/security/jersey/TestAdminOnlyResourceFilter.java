package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.AuthenticationRequiredException;
import com.atlassian.plugins.rest.common.security.AuthorisationException;
import com.atlassian.sal.api.user.UserManager;
import com.sun.jersey.spi.container.ContainerRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TestAdminOnlyResourceFilter {
    private AdminOnlyResourceFilter adminOnlyResourceFilter;
    @Mock
    private UserManager mockUserManager;
    @Mock
    private ContainerRequest containerRequest;

    @Before
    public void setUp() {
        initMocks(this);
        adminOnlyResourceFilter = new AdminOnlyResourceFilter(mockUserManager);
    }

    @Test
    public void filterPassed() {
        when(mockUserManager.getRemoteUsername()).thenReturn("dusan");
        when(mockUserManager.isAdmin("dusan")).thenReturn(true);
        assertSame(containerRequest, adminOnlyResourceFilter.getRequestFilter().filter(containerRequest));
        verify(mockUserManager).isAdmin("dusan");
    }

    @Test(expected = AuthenticationRequiredException.class)
    public void filterRejectedNoLogin() {
        adminOnlyResourceFilter.getRequestFilter().filter(containerRequest);
    }


    @Test
    public void filterRejectedNotAdmin() {
        when(mockUserManager.getRemoteUsername()).thenReturn("dusan");
        try {
            adminOnlyResourceFilter.getRequestFilter().filter(containerRequest);
            fail("AuthorisationException not thrown");
        } catch (AuthorisationException ae) {
            verify(mockUserManager).isAdmin("dusan");
        }
    }

}
