package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.AuthorisationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
public class TestAuthorisationExceptionMapper {
    private AuthorisationExceptionMapper mapper;

    @Mock
    private Request request;


    @Before
    public void setupMocks() {
        mapper = new AuthorisationExceptionMapper();
        mapper.request = request;
    }

    @Test
    public void exceptionAlwaysMapsToUnauthorized() {
        AuthorisationException e = new AuthorisationException("Client not authorized");
        Response resp = mapper.toResponse(e);
        assertEquals("Response status should be forbidden", 403, resp.getStatus());
    }
}
