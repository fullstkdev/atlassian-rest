package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.XsrfCheckFailedException;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;
import com.google.common.collect.ImmutableList;
import com.sun.jersey.spi.container.ContainerRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.util.Arrays;

import static com.atlassian.plugins.rest.common.security.jersey.TestXsrfResourceFilter.addTokenHeaderToRequest;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestOriginBasedXsrfResourceFilter {
    private static final String IE_8_UA = "Mozilla/4.0 (compatible; MSIE 8.0" +
            "; Windows NT 6.0; Trident/4.0)";
    private static final String IE_10_UA = "Mozilla/5.0 (compatible; " +
            "MSIE 10.0; Windows NT 6.2; Trident/6.0)";
    private static final String IE_11_UA = "Mozilla/5.0 " +
            "(Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko";
    private static final String CHROME_32_UA = "Mozilla/5.0 (Windows NT 6.2;" +
            " Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/32.0.1667.0 Safari/537.36";

    private static final ImmutableList<String> IDEMPOTENT_METHODS = ImmutableList.of(
        HttpMethod.GET, HttpMethod.HEAD, HttpMethod.OPTIONS, "TRACE");

    private XsrfResourceFilter xsrfResourceFilter;
    @Mock
    private ContainerRequest request;
    @Mock
    private HttpContext httpContext;
    @Mock
    private HttpServletRequest httpServletRequest;

    @Before
    public void setUp() throws Exception {
        xsrfResourceFilter = new OriginBasedXsrfResourceFilter();
        xsrfResourceFilter.setXsrfRequestValidator(
                new XsrfRequestValidatorImpl(
                        new IndependentXsrfTokenValidator(
                                new IndependentXsrfTokenAccessor())));
        when(request.getRequestUri()).thenReturn(new URI("http://default.com"));
        when(request.getPath()).thenReturn("/example");
        when(request.getMethod()).thenReturn("POST");
        when(httpContext.getRequest()).thenReturn(httpServletRequest);
        xsrfResourceFilter.setHttpContext(httpContext);
    }

    @Test
    public void browserIdempotentRequestNotBlocked() throws Exception {
        setBrowserUserAgentInRequest(request);
        for (String method : IDEMPOTENT_METHODS) {
            when(request.getMethod()).thenReturn(method);
            xsrfResourceFilter.filter(request);
        }
    }

    @Test
    public void nonBrowserIdempotentRequestNotBlocked() throws Exception {
        for (String method : IDEMPOTENT_METHODS) {
            when(request.getMethod()).thenReturn(method);
            xsrfResourceFilter.filter(request);
        }
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void browserPostJsonRequestBlocked() throws Exception {
        when(request.getMethod()).thenReturn("POST");
        filterBrowserJsonRequest(request);
    }

    @Test
    public void browserPutJsonRequestNotBlocked() throws Exception {
        when(request.getMethod()).thenReturn("PUT");
        filterBrowserJsonRequest(request);
    }

    @Test
    public void browserDeleteJsonRequestNotBlocked() throws Exception {
        when(request.getMethod()).thenReturn("DELETE");
        filterBrowserJsonRequest(request);
    }

    @Test
    public void nonBrowserPostJsonRequestNotBlocked() throws Exception {
        setJsonContentTypeForRequest(request);
        xsrfResourceFilter.filter(request);
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void NonIEBrowserPostJsonRequestWithTokenHeaderBlocked()
            throws Exception {
        addTokenHeaderToRequest(httpServletRequest, XsrfResourceFilter.NO_CHECK);
        setJsonContentTypeForRequest(request);
        setBrowserUserAgentInRequest(request, CHROME_32_UA);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void internetExplorerBrowserPostJsonRequestWithTokenHeaderNotBlocked()
            throws Exception {
        addTokenHeaderToRequest(httpServletRequest, XsrfResourceFilter.NO_CHECK);
        setJsonContentTypeForRequest(request);
        for (String userAgent : Arrays.asList(IE_8_UA, IE_10_UA, IE_11_UA)) {
            setBrowserUserAgentInRequest(request, userAgent);
            xsrfResourceFilter.filter(request);
        }
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void browserPostNoContentTypeRequestBlocked() throws Exception {
        setBrowserUserAgentInRequest(request);
        setRequestContentType(request, null);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void nonBrowserPostNoContentTypeRequestNotBlocked() throws Exception {
        setRequestContentType(request, null);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void browserPostXsrfContentTypeRequestNotBlocked() throws Exception {
        setBrowserUserAgentInRequest(request);
        setXsrfableContentType(request);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void nonBrowserPostXsrfContentTypeRequestNotBlocked() throws Exception {
        setXsrfableContentType(request);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void browserDeleteXsrfContentTypeRequestNotBlocked() throws Exception {
        when(request.getMethod()).thenReturn("DELETE");
        setBrowserUserAgentInRequest(request);
        setXsrfableContentType(request);
        xsrfResourceFilter.filter(request);
    }

    private void filterBrowserJsonRequest(ContainerRequest request)
            throws Exception {
        setBrowserUserAgentInRequest(request);
        setJsonContentTypeForRequest(request);
        xsrfResourceFilter.filter(request);
    }

    static void setXsrfableContentType(ContainerRequest request) {
        setRequestContentType(request, MediaType.TEXT_PLAIN_TYPE);
    }

    private static void setJsonContentTypeForRequest(ContainerRequest request) {
        setRequestContentType(request, MediaType.APPLICATION_JSON_TYPE);
    }

    private static void setRequestContentType(ContainerRequest request,
                                              MediaType mediaType) {
        when(request.getMediaType()).thenReturn(mediaType);
    }

    static void setBrowserUserAgentInRequest(ContainerRequest request) {
        setBrowserUserAgentInRequest(request, CHROME_32_UA);
    }

    private static void setBrowserUserAgentInRequest(ContainerRequest request, String userAgent) {
        when(request.getHeaderValue("User-Agent")).thenReturn(userAgent);
    }

}
