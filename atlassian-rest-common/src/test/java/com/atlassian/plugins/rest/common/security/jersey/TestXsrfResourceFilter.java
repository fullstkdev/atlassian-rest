package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugins.rest.common.security.XsrfCheckFailedException;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaultsModuleDescriptor;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.spi.container.ContainerRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 */
@RunWith(MockitoJUnitRunner.class)
public class TestXsrfResourceFilter {
    private static final String URI_ONE = "http://foo.com/hello";
    private static final String URI_TWO = "http://bar.com/hello";
    private static final String URI_ONE_HTTPS = "https://foo.com/hello";
    private static final String URI_ONE_CUSTOM_PORT = "http://foo.com:1234/hello";
    private static final String URI_ONE_NO_PATH = "http://foo.com";
    private static final String URI_ONE_INVALID_QUERY_PARAM = URI_ONE + "?|";
    private static final String INVALID_URI = "!!! oh noes, invalid URI :O";
    private static final String URI_CHROME_EXTENSION = "chrome-extension://foobar";
    private static final String URI_SAFARI_EXTENSION = "safari-extension://foobar";

    private XsrfResourceFilter xsrfResourceFilter;
    @Mock
    private ContainerRequest request;
    @Mock
    private HttpContext httpContext;
    @Mock
    private HttpServletRequest httpServletRequest;

    @Before
    public void setUp() throws Exception {
        xsrfResourceFilter = new XsrfResourceFilter();
        xsrfResourceFilter.setXsrfRequestValidator(
                new XsrfRequestValidatorImpl(
                        new IndependentXsrfTokenValidator(
                                new IndependentXsrfTokenAccessor())));
        when(httpContext.getRequest()).thenReturn(httpServletRequest);
        xsrfResourceFilter.setHttpContext(httpContext);

        when(request.getRequestUri()).thenReturn(new URI("http://default.com"));
        when(request.getPath()).thenReturn("/example");
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testGetProtectedByXsrfChecks() {
        when(request.getMethod()).thenReturn("GET");
        addTokenHeaderToRequest(httpServletRequest, "invalid");
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testPostBlocked() {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void testPostSuccessWithCorrectHeaderValue() {
        when(request.getMediaType()).thenReturn(
                MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(request.getMethod()).thenReturn("POST");
        addTokenHeaderToRequest(httpServletRequest,
                XsrfResourceFilter.NO_CHECK);
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test
    public void testPostSuccessWithOldHeaderValue() {
        final String deprecatedHeaderValue = "nocheck";
        when(request.getMediaType()).thenReturn(
                MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(request.getMethod()).thenReturn("POST");
        when(request.getHeaderValue(XsrfResourceFilter.TOKEN_HEADER)).
                thenReturn(deprecatedHeaderValue);
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testPostFailureWithInvalidHeaderValue() {
        when(request.getMediaType()).thenReturn(
                MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(request.getMethod()).thenReturn("POST");
        addTokenHeaderToRequest(httpServletRequest, "invalid");
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test
    public void testPostJsonSuccess() {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_JSON_TYPE);
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test
    public void testPutFormNotBlocked() {
        when(request.getMethod()).thenReturn("PUT");
        when(request.getMediaType()).thenReturn(
                MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        assertEquals(request, xsrfResourceFilter.filter(request));
    }

    @Test
    public void testPostWithValidXsrfTokenSuccess() {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        for (String validToken : Arrays.asList("abc123", "ffff", "FFFx3~324", "☃☃☃")) {
            httpServletRequest = setupRequestWithXsrfToken(httpServletRequest, validToken, validToken);
            assertThat(xsrfResourceFilter.filter(request), is(request));
        }
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testPostWithInvalidXsrfTokenBlocked() {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        httpServletRequest = setupRequestWithXsrfToken(httpServletRequest, "abc123", "notabc123");
        xsrfResourceFilter.filter(request);
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void testPostWithNullHttpServletRequestXsrfTokenBlocked() {
        when(request.getMethod()).thenReturn("POST");
        when(request.getMediaType()).thenReturn(
                MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(httpContext.getRequest()).thenReturn(null);
        xsrfResourceFilter.setHttpContext(httpContext);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void httpServletRequestMethodPreferedOverContainerRequest() {
        when(request.getMethod()).thenReturn(HttpMethod.PUT);
        when(httpServletRequest.getMethod()).thenReturn(HttpMethod.OPTIONS);

        xsrfResourceFilter.filter(request);
    }

    @Test(expected = XsrfCheckFailedException.class)
    public void browserPostXsrfContentTypeRequestBlocked() throws Exception {
        when(request.getMethod()).thenReturn("POST");
        TestOriginBasedXsrfResourceFilter.setBrowserUserAgentInRequest(request);
        TestOriginBasedXsrfResourceFilter.setXsrfableContentType(request);
        xsrfResourceFilter.filter(request);
    }

    @Test
    public void browserDeleteXsrfContentTypeRequestNotBlocked() throws Exception {
        when(request.getMethod()).thenReturn("DELETE");
        TestOriginBasedXsrfResourceFilter.setBrowserUserAgentInRequest(request);
        TestOriginBasedXsrfResourceFilter.setXsrfableContentType(request);
        xsrfResourceFilter.filter(request);
    }

    static void addTokenHeaderToRequest(
            HttpServletRequest httpServletRequest, String headerValue) {
        when(httpServletRequest.getHeader(XsrfResourceFilter.TOKEN_HEADER))
                .thenReturn(headerValue);
    }

    private static HttpServletRequest setupRequestWithXsrfToken(
            HttpServletRequest request, String expectedToken, String formToken) {
        Cookie[] cookies = {new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, expectedToken)};
        when(request.getCookies()).thenReturn(cookies);
        when(request.getParameter(IndependentXsrfTokenValidator.XSRF_PARAM_NAME)).thenReturn(formToken);
        return request;
    }

    @Test
    public void additionalBrowserChecksWithNullOriginAndReferrerFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, null, null));
    }

    @Test
    public void additionalBrowserChecksWithEmptyOriginAndReferrerFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, "", ""));
    }

    @Test
    public void additionalBrowserChecksWithInvalidOriginAndReferrerFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, INVALID_URI, INVALID_URI));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginPass() throws Exception {
        assertTrue(runAdditionalBrowserChecks(URI_ONE, URI_ONE, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginAndDifferentPathPass() throws Exception {
        assertTrue(runAdditionalBrowserChecks(URI_ONE, URI_ONE_NO_PATH, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginButDifferentSecurityLevelFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE_HTTPS, URI_ONE, URI_TWO));
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_ONE_HTTPS, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginButDifferentPortsFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_ONE_CUSTOM_PORT, URI_TWO));
        assertFalse(runAdditionalBrowserChecks(URI_ONE_CUSTOM_PORT, URI_ONE, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithSameReferrerPass() throws Exception {
        assertTrue(runAdditionalBrowserChecks(URI_ONE, URI_TWO, URI_ONE));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginReferrerWithInvalidQueryParamPass() throws Exception {
        assertTrue(runAdditionalBrowserChecks(URI_ONE, "", URI_ONE_INVALID_QUERY_PARAM));
    }

    @Test
    public void additionalBrowserChecksWithSameReferrerButDifferentSecurityLevelFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE_HTTPS, URI_TWO, URI_ONE));
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_TWO, URI_ONE_HTTPS));
    }

    @Test
    public void additionalBrowserChecksWithSameReferrerButDifferentPortsFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_TWO, URI_ONE_CUSTOM_PORT));
        assertFalse(runAdditionalBrowserChecks(URI_ONE_CUSTOM_PORT, URI_TWO, URI_ONE));
    }

    @Test
    public void additionalBrowserChecksWithDifferentOriginAndReferrerFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_TWO, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithBrowserExtensionOriginPass() throws Exception {
        assertThat(runAdditionalBrowserChecks(URI_ONE, URI_CHROME_EXTENSION, ""), is(true));
        assertThat(runAdditionalBrowserChecks(URI_ONE_HTTPS, URI_SAFARI_EXTENSION, ""), is(true));
    }

    private boolean runAdditionalBrowserChecks(final String uri, final String origin, final String referrer) throws Exception {
        xsrfResourceFilter.setPluginModuleTracker(emptyPluginModuleTracker());

        when(request.getHeaderValue("Origin")).thenReturn(origin);
        when(request.getHeaderValue("Referer")).thenReturn(referrer);
        when(request.getRequestUri()).thenReturn(new URI(uri));

        return xsrfResourceFilter.passesAdditionalBrowserChecks(request);
    }

    @Test
    public void additionalBrowserChecksPassIfCorsCredentialsNotAllowedAndNotSupplied() {
        xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker(URI_ONE, true, false));

        when(request.getHeaderValue("Origin")).thenReturn(URI_ONE);

        assertTrue(xsrfResourceFilter.passesAdditionalBrowserChecks(request));
    }

    @Test
    public void additionalBrowserChecksPassIfCorsCredentialsAllowedAndNotSupplied() {
        xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker(URI_ONE, true, true));

        when(request.getHeaderValue("Origin")).thenReturn(URI_ONE);

        assertTrue(xsrfResourceFilter.passesAdditionalBrowserChecks(request));
    }

    @Test
    public void additionalBrowserChecksPassIfCorsCredentialsAllowedAndCookiesSupplied() {
        xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker(URI_ONE, true, true));

        when(request.getHeaderValue("Origin")).thenReturn(URI_ONE);
        setArbitraryCookie(request);

        assertTrue(xsrfResourceFilter.passesAdditionalBrowserChecks(request));
    }

    @Test
    public void additionalBrowserChecksPassIfCorsCredentialsAllowedAndHttpAuthHeaderSupplied() {
        xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker(URI_ONE, true, true));

        when(request.getHeaderValue("Origin")).thenReturn(URI_ONE);
        when(request.getHeaderValue("Authorization")).thenReturn("Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==");

        assertTrue(xsrfResourceFilter.passesAdditionalBrowserChecks(request));
    }

    @Test
    public void additionalBrowserChecksFailIfCorsCredentialsDisallowedButCookiesSupplied() {
        xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker(URI_ONE, true, false));

        when(request.getHeaderValue("Origin")).thenReturn(URI_ONE);
        setArbitraryCookie(request);

        assertFalse(xsrfResourceFilter.passesAdditionalBrowserChecks(request));
    }

    @Test
    public void additionalBrowserChecksFailIfCorsCredentialsDisallowedButHttpAuthHeaderSupplied() {
        xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker(URI_ONE, true, false));

        when(request.getHeaderValue("Origin")).thenReturn(URI_ONE);
        when(request.getHeaderValue("Authorization")).thenReturn("Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==");

        assertFalse(xsrfResourceFilter.passesAdditionalBrowserChecks(request));
    }

    @Test
    public void additionalBrowserChecksTakePortNumbersIntoAccountWhenCheckingAllowedViaCors() {
        xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker(URI_ONE, true, false));

        when(request.getHeaderValue("Origin")).thenReturn(URI_ONE_CUSTOM_PORT);

        assertFalse(xsrfResourceFilter.passesAdditionalBrowserChecks(request));
    }

    @Test
    public void additionalBrowserChecksTakeHttpHttpsIntoAccountWhenCheckingAllowedViaCors() {
        xsrfResourceFilter.setPluginModuleTracker(pluginModuleTracker(URI_ONE, true, false));

        when(request.getHeaderValue("Origin")).thenReturn(URI_ONE_HTTPS);

        assertFalse(xsrfResourceFilter.passesAdditionalBrowserChecks(request));
    }

    /**
     * Builds a plugin module tracker that contains no modules (and hence disallows all CORS requests).
     *
     * @return plugin module tracker with no modules
     */
    private PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> emptyPluginModuleTracker() {
        final PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> pluginModuleTracker = mock(PluginModuleTracker.class);
        when(pluginModuleTracker.getModules()).thenReturn(Collections.EMPTY_LIST);
        return pluginModuleTracker;
    }

    /**
     * Builds a plugin module tracker containing a module that will allow/disallow a CORS request.
     *
     * @param originUri          the origin making the request
     * @param originAllowed      true if the module should allow the origin
     * @param credentialsAllowed true if the module should allow credentials to be included in requests from the origin
     * @return plugin module tracker containing a module that allows/disallows requests from the origin
     * based on the {@code originAllowed} and {@code credentialsAllowed} parameters
     */
    private PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> pluginModuleTracker(
            final String originUri,
            final boolean originAllowed,
            final boolean credentialsAllowed) {
        final CorsDefaults corsDefaults = mock(CorsDefaults.class);
        when(corsDefaults.allowsOrigin(originUri)).thenReturn(originAllowed);
        when(corsDefaults.allowsCredentials(originUri)).thenReturn(credentialsAllowed);

        final PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> pluginModuleTracker = mock(PluginModuleTracker.class);
        when(pluginModuleTracker.getModules()).thenReturn(ImmutableList.of(corsDefaults));

        return pluginModuleTracker;
    }

    private void setArbitraryCookie(final ContainerRequest request) {
        when(request.getCookies()).thenReturn(ImmutableMap.of("foo", new javax.ws.rs.core.Cookie("bar", "baz")));
    }
}
