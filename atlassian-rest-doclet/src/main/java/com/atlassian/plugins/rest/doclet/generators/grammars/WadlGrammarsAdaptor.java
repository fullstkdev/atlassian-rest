package com.atlassian.plugins.rest.doclet.generators.grammars;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.api.model.AbstractResource;
import com.sun.jersey.api.model.AbstractResourceMethod;
import com.sun.jersey.api.model.Parameter;
import com.sun.jersey.server.wadl.ApplicationDescription;
import com.sun.jersey.server.wadl.WadlGenerator;

/*
 * Internal class, don't use directly:
 * 
 * This class changes logic of WadlGenerator and tries to parse grammars.xml file into
 * two different naspace. If old namespace detected then parse tree gets converted into
 * new domain objects tree with using GrammarTransformer class
 * 
 * It would be nice to derive this class from WadlGeneratorGrammarsSupport, however all
 * internal state is private in jersey version. So this is direct copy instead
 * 
 * Keep code as close as possible to original class for easier maintanance 
 */
public class WadlGrammarsAdaptor implements WadlGenerator {
    private static final Logger LOG = LoggerFactory.getLogger(WadlGrammarsAdaptor.class);

    private WadlGenerator _delegate;
    private File _grammarsFile;
    private InputStream _grammarsStream;
    private Boolean overrideGrammars = false;

    private com.sun.research.ws.wadl.Grammars _grammars;

    public WadlGrammarsAdaptor() {
    }

    public WadlGrammarsAdaptor(WadlGenerator delegate, com.sun.research.ws.wadl.Grammars grammars) {
        _delegate = delegate;
        _grammars = grammars;
    }

    public void setWadlGeneratorDelegate(WadlGenerator delegate) {
        _delegate = delegate;
    }

    /**
     * Delegates the setting of the environment
     */
    @Override
    public void setEnvironment(Environment env) {
        _delegate.setEnvironment(env);
    }

    public void setOverrideGrammars(Boolean overrideGrammars) {
        this.overrideGrammars = overrideGrammars;
    }

    public String getRequiredJaxbContextPath() {
        return _delegate.getRequiredJaxbContextPath();
    }

    public void setGrammarsFile(File grammarsFile) {
        if (_grammarsStream != null) {
            throw new IllegalStateException("The grammarsStream property is already set,"
                    + " therefore you cannot set the grammarsFile property. Only one of both can be set at a time.");
        }
        _grammarsFile = grammarsFile;
    }

    public void setGrammarsStream(InputStream grammarsStream) {
        if (_grammarsFile != null) {
            throw new IllegalStateException("The grammarsFile property is already set,"
                    + " therefore you cannot set the grammarsStream property. Only one of both can be set at a time.");
        }
        _grammarsStream = grammarsStream;
    }

    public void init() throws Exception {
        if (_grammarsFile == null && _grammarsStream == null) {
            throw new IllegalStateException("Neither the grammarsFile nor the grammarsStream"
                    + " is set, one of both is required.");
        }
        _delegate.init();

        // Changed by atlassian
        // REST-242: try different XML namespaces (see package-info notes)
        final JAXBContext c = JAXBContext.newInstance(com.sun.research.ws.wadl.Grammars.class, Grammars.class);
        final Unmarshaller m = c.createUnmarshaller();
        final Object obj = _grammarsFile != null ? m.unmarshal(_grammarsFile) : m.unmarshal(_grammarsStream);

        if (obj.getClass() == Grammars.class) {
            Grammars grm = Grammars.class.cast(obj);
            _grammars = GrammarTransformer.transform(grm);
        } else if (obj.getClass() == com.sun.research.ws.wadl.Grammars.class) {
            _grammars = com.sun.research.ws.wadl.Grammars.class.cast(obj);
        } else {
            // Impossible, only present for if-else completeness
            throw new RuntimeException("Unknown grammars class: " + obj.getClass());
        }
    }


    /**
     * @return application
     * @see com.sun.jersey.server.wadl.WadlGenerator#createApplication()
     */
    public com.sun.research.ws.wadl.Application createApplication(UriInfo requestInfo) {
        final com.sun.research.ws.wadl.Application result = _delegate.createApplication(requestInfo);
        if (result.getGrammars() != null && !overrideGrammars) {
            LOG.info("The wadl application created by the delegate (" + _delegate
                    + ") already contains a grammars element,"
                    + " we're adding elements of the provided grammars file.");
            if (!_grammars.getAny().isEmpty()) {
                result.getGrammars().getAny().addAll(_grammars.getAny());
            }
            if (!_grammars.getDoc().isEmpty()) {
                result.getGrammars().getDoc().addAll(_grammars.getDoc());
            }
            if (!_grammars.getInclude().isEmpty()) {
                result.getGrammars().getInclude().addAll(_grammars.getInclude());
            }
        } else {
            result.setGrammars(_grammars);
        }
        return result;
    }

    /**
     * @param ar  abstract resource
     * @param arm abstract resource method
     * @return method
     * @see com.sun.jersey.server.wadl.WadlGenerator#createMethod(com.sun.jersey.api.model.AbstractResource,
     * com.sun.jersey.api.model.AbstractResourceMethod)
     */
    public com.sun.research.ws.wadl.Method createMethod(AbstractResource ar, AbstractResourceMethod arm) {
        return _delegate.createMethod(ar, arm);
    }

    /**
     * @param ar  abstract resource
     * @param arm abstract resource method
     * @return request
     * @see com.sun.jersey.server.wadl.WadlGenerator#createRequest(com.sun.jersey.api.model.AbstractResource,
     * com.sun.jersey.api.model.AbstractResourceMethod)
     */
    public com.sun.research.ws.wadl.Request createRequest(AbstractResource ar, AbstractResourceMethod arm) {
        return _delegate.createRequest(ar, arm);
    }

    /**
     * @param ar abstract resource
     * @param am abstract method
     * @param p  parameter
     * @return parameter
     * @see com.sun.jersey.server.wadl.WadlGenerator#createParam(com.sun.jersey.api.model.AbstractResource,
     * com.sun.jersey.api.model.AbstractMethod, com.sun.jersey.api.model.Parameter)
     */
    public com.sun.research.ws.wadl.Param createParam(AbstractResource ar, AbstractMethod am, Parameter p) {
        return _delegate.createParam(ar, am, p);
    }

    /**
     * @param ar  abstract resource
     * @param arm abstract resource method
     * @param mt  media type
     * @return respresentation type
     * @see com.sun.jersey.server.wadl.WadlGenerator#createRequestRepresentation(com.sun.jersey.api.model.AbstractResource,
     * com.sun.jersey.api.model.AbstractResourceMethod, javax.ws.rs.core.MediaType)
     */
    public com.sun.research.ws.wadl.Representation createRequestRepresentation(AbstractResource ar, AbstractResourceMethod arm, MediaType mt) {
        return _delegate.createRequestRepresentation(ar, arm, mt);
    }

    /**
     * @param ar   abstract resource
     * @param path
     * @return resource
     * @see com.sun.jersey.server.wadl.WadlGenerator#createResource(com.sun.jersey.api.model.AbstractResource,
     * java.lang.String)
     */
    public com.sun.research.ws.wadl.Resource createResource(AbstractResource ar, String path) {
        return _delegate.createResource(ar, path);
    }

    /**
     * @return resources
     * @see com.sun.jersey.server.wadl.WadlGenerator#createResources()
     */
    public com.sun.research.ws.wadl.Resources createResources() {
        return _delegate.createResources();
    }

    /**
     * @param ar  abstract resource
     * @param arm abstract resource method
     * @return response
     * @see com.sun.jersey.server.wadl.WadlGenerator#createResponses(com.sun.jersey.api.model.AbstractResource,
     * com.sun.jersey.api.model.AbstractResourceMethod)
     */
    public List<com.sun.research.ws.wadl.Response> createResponses(AbstractResource ar, AbstractResourceMethod arm) {
        return _delegate.createResponses(ar, arm);
    }

    // ================ methods for post build actions =======================

    @Override
    public ExternalGrammarDefinition createExternalGrammar() {
        if (overrideGrammars) {
            return new ExternalGrammarDefinition();
        }
        return _delegate.createExternalGrammar();
    }

    @Override
    public void attachTypes(ApplicationDescription egd) {
        _delegate.attachTypes(egd);
    }
}
