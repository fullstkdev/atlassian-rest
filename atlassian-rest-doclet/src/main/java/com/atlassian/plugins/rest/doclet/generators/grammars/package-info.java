/*
 * !!!INTERNAL STAFF, DON'T USE DIRECTLY!!!
 * 
 * REST-242:
 * 
 * Reason:
 * Migration to the upstream rest becomes very painful because XML namespace in grammars.xml got changed. That 
 * leads to situation when _all_ dependent plugins should be rebuild to regenerate XML in order to be loaded
 * 
 * Solution:
 * Jersey loads xml with using JAXB, for that a number of domain objects generated from Schema. Old and
 * new domain objects are exactly same, except schema url. To avoid the conflict WadlGrammarsAdaptor
 * trys to load grammars.xml for both namespaces. Old namespace is present by the local copy of old domain
 * objects in the current package. 
 * 
 * In case when old version of objects (namespace) detected the final tree gets copied into new version
 * of domain objects (see GrammarTransform class) before gets return to Jersey  
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://research.sun.com/wadl/2006/10", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.atlassian.plugins.rest.doclet.generators.grammars;
