package com.atlassian.plugins.rest.doclet.generators.resourcedoc;

import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.plugins.rest.common.version.ApiVersion;
import com.atlassian.plugins.rest.doclet.generators.schema.RichClass;
import com.atlassian.plugins.rest.doclet.generators.schema.SchemaGenerator;
import com.atlassian.rest.annotation.ExcludeFromDoc;
import com.atlassian.rest.annotation.RestProperty;
import com.google.common.collect.Lists;
import com.sun.jersey.api.model.AbstractResource;
import com.sun.jersey.api.model.AbstractResourceMethod;
import com.sun.jersey.server.wadl.WadlGenerator;
import com.sun.jersey.server.wadl.generators.resourcedoc.WadlGeneratorResourceDocSupport;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.xhtml.Elements;
import com.sun.research.ws.wadl.Doc;
import com.sun.research.ws.wadl.Method;
import com.sun.research.ws.wadl.Representation;
import com.sun.research.ws.wadl.Resource;
import com.sun.research.ws.wadl.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.ws.rs.core.MediaType;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.annotations.tenancy.TenancyScope.TENANTLESS;
import static com.atlassian.plugins.rest.doclet.generators.resourcedoc.JsonOperations.toJson;
import static com.atlassian.plugins.rest.doclet.generators.resourcedoc.RestMethod.restMethod;

/**
 * This class generates the WADL description of rest resources and considers the rest plugin module descriptors
 * configured inside the atlassian-plugin.xml file when generating the resource path.
 * <p/>
 * It builds up a map that contains a mapping of a package name to a resource path.
 * The full resource path is concatenated of the following strings:
 * 1) path as configured for rest plugin module descriptor: e.g. api
 * 2) version as configured for rest plugin module descriptor e.g. 2.0.alpha1
 * 3) path of the rest end point e.g. worklog
 *
 * <p/>
 * e.g. /api/2.0.alpha1/worklog/
 */
public class AtlassianWadlGeneratorResourceDocSupport extends WadlGeneratorResourceDocSupport {
    @TenantAware(TENANTLESS)
    private HashMap<String, ResourcePathInformation> resourcePathInformation;

    private static final Logger LOG = LoggerFactory.getLogger(AtlassianWadlGeneratorResourceDocSupport.class);
    private static final String ATLASSIAN_PLUGIN_XML = "atlassian-plugin.xml";
    private boolean generateSchemas = true;

    public AtlassianWadlGeneratorResourceDocSupport() {
        super();
    }

    public AtlassianWadlGeneratorResourceDocSupport(WadlGenerator wadlGenerator, ResourceDocType resourceDoc) {
        super(wadlGenerator, resourceDoc);
    }

    @Override
    public void init() throws Exception {
        super.init();
        parseAtlassianPluginXML();
    }

    public void setGenerateSchemas(Boolean generateSchemas) {
        this.generateSchemas = generateSchemas;
    }

    private void parseAtlassianPluginXML() {
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            final URL resource = getClass().getClassLoader().getResource(ATLASSIAN_PLUGIN_XML);
            if (resource == null) {
                return;
            }
            LOG.info("Found " + ATLASSIAN_PLUGIN_XML + " file! Looking for rest plugin module descriptors...");

            DocumentBuilder db = dbf.newDocumentBuilder();

            resourcePathInformation = new HashMap<String, ResourcePathInformation>();

            final Document document = db.parse(resource.toExternalForm());
            final NodeList restPluginModuleDescriptors = document.getElementsByTagName("rest");
            final int numPluginModuleDescriptors = restPluginModuleDescriptors.getLength();
            LOG.info("Found " + numPluginModuleDescriptors + " rest plugin module descriptors.");

            for (int i = 0; i < numPluginModuleDescriptors; i++) {
                final Node node = restPluginModuleDescriptors.item(i);

                final NamedNodeMap attributes = node.getAttributes();
                final Node pathItem = attributes.getNamedItem("path");
                final Node versionItem = attributes.getNamedItem("version");
                if (pathItem == null || versionItem == null) {
                    continue;
                }

                String resourcePath = pathItem.getNodeValue();
                String version = versionItem.getNodeValue();

                LOG.info("Found rest end point with path '" + resourcePath + "' and version '" + version + "'");

                //Remove leading slash
                if (resourcePath.indexOf("/") != -1) {
                    resourcePath = resourcePath.substring(resourcePath.indexOf("/") + 1);
                }

                final NodeList list = node.getChildNodes();
                for (int j = 0; j < list.getLength(); j++) {
                    final Node child = list.item(j);
                    if (child.getNodeName().equals("package")) {
                        final String packageName = child.getFirstChild().getNodeValue();
                        LOG.info("Map package '" + packageName + "' to resource path '" + resourcePath + "' and version '" + version + "'");
                        resourcePathInformation.put(packageName, new ResourcePathInformation(resourcePath, version));
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error("Failed to read " + ATLASSIAN_PLUGIN_XML + " and parse rest plugin module descriptor information. Reason", ex);
        }
    }

    @Override
    public Resource createResource(AbstractResource r, String path) {

        removeMethodsExcludedFromDocs(r);
        if (allMethodsExcluded(r)) {
            return new Resource();
        }

        final Resource result = super.createResource(r, path);
        boolean resourcePathChanged = false;
        for (String packageName : resourcePathInformation.keySet()) {
            if (r.getResourceClass().getPackage().getName().startsWith(packageName)) {
                final ResourcePathInformation pathInformation = resourcePathInformation.get(packageName);
                final String newPath = buildResourcePath(result, pathInformation);
                result.setPath(newPath);
                resourcePathChanged = true;
                LOG.info("Setting resource path of rest end point '" + r.getResourceClass().getCanonicalName() + "' to '" + newPath + "'");
                break;
            }
        }
        if (!resourcePathChanged) {
            LOG.info("Resource path of rest end point '" + r.getResourceClass().getCanonicalName() + "' unchanged no mapping to rest plugin module descriptor found.");
        }
        return result;
    }

    private boolean allMethodsExcluded(AbstractResource r) {
        return r.getResourceMethods().isEmpty() && r.getSubResourceMethods().isEmpty();
    }

    private void removeMethodsExcludedFromDocs(AbstractResource r) {
        Set<AbstractResourceMethod> excludedMethods = Stream.concat(r.getResourceMethods().stream().filter(method -> isMethodExcluded(r, method)),
                r.getSubResourceMethods().stream().filter(method -> isMethodExcluded(r, method))).collect(Collectors.toSet());

        excludedMethods.forEach(method -> {
            r.getResourceMethods().remove(method);
            r.getSubResourceMethods().remove(method);
        });
    }

    private boolean isMethodExcluded(AbstractResource r, AbstractResourceMethod method) {
        return method.isAnnotationPresent(ExcludeFromDoc.class) || r.getResourceClass().isAnnotationPresent(ExcludeFromDoc.class);
    }

    private String buildResourcePath(Resource result, ResourcePathInformation pathInformation) {
        if (ApiVersion.isNone(pathInformation.getVersion())) {
            return pathInformation.getPath() + "/" + result.getPath();
        } else {
            return pathInformation.getPath() + "/" + pathInformation.getVersion() + "/" + result.getPath();
        }
    }

    @Override
    public Method createMethod(final AbstractResource r, final AbstractResourceMethod m) {
        final Method method = super.createMethod(r, m);
        RestMethod restMethod = restMethod(r.getResourceClass(), m.getMethod());
        if (restMethod.isExperimental()) {
            method.getOtherAttributes().put(new QName("experimental"), Boolean.TRUE.toString());
        }
        if (restMethod.isDeprecated()) {
            method.getOtherAttributes().put(new QName("deprecated"), Boolean.TRUE.toString());
        }
        return method;
    }

    @Override
    public Representation createRequestRepresentation(final AbstractResource r, final AbstractResourceMethod m, final MediaType mediaType) {
        final Representation representation = super.createRequestRepresentation(r, m, mediaType);
        if (generateSchemas) {
            restMethod(r.getResourceClass(), m.getMethod()).getRequestType().ifPresent(richClass ->
                    representation.getDoc().add(schemaDoc(richClass, RestProperty.Scope.REQUEST)));
        }

        return representation;
    }

    @Override
    public List<Response> createResponses(final AbstractResource r, final AbstractResourceMethod m) {
        List<Response> result = Lists.newArrayList();
        for (Response response : super.createResponses(r, m)) {
            if (generateSchemas) {
                addSchemaIfDefinedForStatus(r, m, response);
            }
            result.add(response);
        }

        return result;
    }

    private void addSchemaIfDefinedForStatus(AbstractResource resource, final AbstractResourceMethod method, final Response response) {
        for (Long status : response.getStatus()) {
            for (RichClass responseType : restMethod(resource.getResourceClass(), method.getMethod()).responseTypesFor(status.intValue())) {
                for (Representation representation : response.getRepresentation()) {
                    representation.getDoc().add(schemaDoc(responseType, RestProperty.Scope.RESPONSE));
                }
            }
        }
    }

    private Doc schemaDoc(RichClass model, final RestProperty.Scope scope) {
        String schema = toJson(SchemaGenerator.generateSchema(model, scope));
        final Doc doc = new Doc();

        final Elements element = Elements.el("p")
                .add(Elements.val("h6", "Schema"))
                .add(Elements.el("pre").add(Elements.val("code", schema)));

        doc.getContent().add(element);

        return doc;
    }

    public class ResourcePathInformation {
        private final String path;
        private final String version;

        public ResourcePathInformation(String path, String version) {
            this.path = path;
            this.version = version;
        }

        public String getVersion() {
            return version;
        }

        public String getPath() {
            return path;
        }
    }
}
