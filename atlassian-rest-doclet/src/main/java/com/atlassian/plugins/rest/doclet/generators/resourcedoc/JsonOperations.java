package com.atlassian.plugins.rest.doclet.generators.resourcedoc;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import java.io.IOException;

public class JsonOperations
{
    private final static ObjectMapper objectMapper = new ObjectMapper();

    static
    {
        objectMapper.configure(SerializationConfig.Feature.WRITE_NULL_PROPERTIES, false);
    }

    public static String toJson(Object bean)
    {
        try
        {
            return objectMapper.writeValueAsString(bean);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
