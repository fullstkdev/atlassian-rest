package com.atlassian.plugins.rest.doclet.generators.resourcedoc;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.core.Response;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugins.rest.doclet.generators.schema.RichClass;
import com.atlassian.rest.annotation.ExcludeFromDoc;
import com.atlassian.rest.annotation.RequestType;
import com.atlassian.rest.annotation.ResponseType;
import com.atlassian.rest.annotation.ResponseTypes;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.rest.annotation.ResponseType.StatusType.SUCCESS;
import static java.util.Optional.empty;

public class RestMethod
{
    private static final Logger log = LoggerFactory.getLogger(RestMethod.class);

    private final Class<?> resourceClass;
    private final Method method;

    private RestMethod(final Class<?> resourceClass, final Method method)
    {
        this.resourceClass = resourceClass;
        this.method = method;
    }

    public boolean isExperimental()
    {
        return method.getAnnotation(ExperimentalApi.class) != null;
    }

    public boolean isDeprecated()
    {
        return method.getAnnotation(Deprecated.class) != null;
    }

    public static RestMethod restMethod(Class<?> resourceClass, final Method method)
    {
        return new RestMethod(resourceClass, method);
    }

    public Optional<RichClass> getRequestType()
    {
        Optional<RichClass> typeFromAnnotation = empty();
        Optional<RichClass> typeFromParameter = empty();

        if (method.isAnnotationPresent(RequestType.class))
        {
            RequestType requestType = method.getAnnotation(RequestType.class);
            typeFromAnnotation = Optional.of(RichClass.of(requestType.value(), requestType.genericTypes()));
        }

        for (int i = 0; i < method.getParameterTypes().length; i++)
        {
            if (method.getParameterAnnotations()[i].length == 0)
            {
                typeFromParameter = Optional.of(RichClass.of(method.getGenericParameterTypes()[i]));
                break;
            }
        }

        if (typeFromAnnotation.isPresent() && !typeFromAnnotation.equals(typeFromParameter))
        {
            log.warn(String.format("Method %s.%s declares request type that is different than the actual request "
                    + "parameter of this method. This may result in inaccurate documentation.", resourceClass.getSimpleName(), method.getName()));
        }

        return typeFromAnnotation.isPresent() ? typeFromAnnotation : typeFromParameter;
    }

    public List<RichClass> responseTypesFor(int status)
    {
        List<RichClass> types = Lists.newArrayList();

        for (ResponseType responseType : declaredResponseTypes())
        {
            if (status == responseType.status() || responseType.statusType().matches(status))
            {
                if (!Void.class.equals(responseType.value()))
                {
                    types.add(RichClass.of(responseType.value(), responseType.genericTypes()));
                }
            }
        }

        if (SUCCESS.matches(status) &&
                !method.getReturnType().equals(Response.class) &&
                !"void".equals(method.getGenericReturnType().getTypeName()))
        {
            RichClass actualReturnType = RichClass.of(method.getGenericReturnType());

            if (!types.isEmpty() && !Collections.singletonList(actualReturnType).equals(types))
            {
                log.warn(String.format("Method %s.%s declares response type for success response that is different than the actual return "
                        + "type of this method. This may result in inaccurate documentation.", resourceClass.getSimpleName(), method.getName()));
            }
            else
            {
                return Collections.singletonList(actualReturnType);
            }
        }

        return ImmutableList.copyOf(types);
    }

    private Iterable<ResponseType> declaredResponseTypes()
    {
        List<ResponseType> responseTypes = Lists.newArrayList();

        responseTypes.addAll(responseTypes(method));
        responseTypes.addAll(responseTypes(resourceClass));

        return responseTypes;
    }

    private Collection<ResponseType> responseTypes(final AnnotatedElement element)
    {
        if (element.isAnnotationPresent(ResponseType.class))
        {
            return Collections.singleton(element.getAnnotation(ResponseType.class));
        }

        if (element.isAnnotationPresent(ResponseTypes.class))
        {
            return Arrays.asList(element.getAnnotation(ResponseTypes.class).value());
        }

        return Collections.emptyList();
    }
}
