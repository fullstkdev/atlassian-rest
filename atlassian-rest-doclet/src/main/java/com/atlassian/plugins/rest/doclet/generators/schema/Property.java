package com.atlassian.plugins.rest.doclet.generators.schema;

import com.google.common.base.Objects;

public final class Property
{
    public final ModelClass model;
    public final String name;
    public final boolean required;

    public Property(final ModelClass model, final String name, final boolean required)
    {
        this.model = model;
        this.name = name;
        this.required = required || model.getActualClass().isPrimitive();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Property that = (Property) o;

        return Objects.equal(this.model, that.model) &&
                Objects.equal(this.name, that.name) &&
                Objects.equal(this.required, that.required);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(model, name, required);
    }

    @Override
    public String toString()
    {
        return "Property["+name+"]";
    }
}
