package com.atlassian.plugins.rest.doclet.generators.schema;

import com.atlassian.annotations.tenancy.TenantAware;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.atlassian.annotations.tenancy.TenancyScope.SUPPRESS;
import static com.atlassian.annotations.tenancy.TenancyScope.TENANTLESS;

@JsonAutoDetect
public class Schema
{
    public enum Type
    {
        Any, Object, Array, Number, Integer, Boolean, String, Uri("string", "uri");

        private final String name;
        private final String format;

        Type(final java.lang.String name, final java.lang.String format)
        {
            this.name = name;
            this.format = format;
        }

        Type()
        {
            this(null, null);
        }

        @Override
        public String toString()
        {
            return Objects.firstNonNull(name, this.name().toLowerCase());
        }

        public java.lang.String format()
        {
            return format;
        }
    }

    private final String $ref;
    private final String id;
    private final String title;
    private final String description;
    private final String type;
    private final String format;
    @TenantAware(TENANTLESS)
    private final Map<String, Schema> properties;
    private final Schema items;
    @JsonProperty ("enum")
    private final List<String> _enum;

    @TenantAware(TENANTLESS)
    private final Map<String, Schema> patternProperties;
    private final List<Schema> anyOf;

    @TenantAware(TENANTLESS)
    private final Map<String, Schema> definitions;
    private Boolean additionalProperties;
    private final List<String> required;

    private Schema(final String $ref, final String id, String title, String description, Type type, Map<String, Schema> properties, Schema items, Iterable<String> _enum, Iterable<String> required, Map<String, Schema> patternProperties, final Map<String, Schema> definitions, final List<Schema> anyOf)
    {
        this.$ref = $ref;
        this.id = id;
        this.title = title;
        this.description = description;
        this.type = type != null && type != Type.Any ? type.toString() : null;
        this.format = this.type != null ? type.format() : null;
        this.anyOf = anyOf != null && anyOf.size() > 0 ? ImmutableList.copyOf(anyOf) : null;
        this.properties = properties != null && properties.size() > 0
                ? ImmutableMap.copyOf(properties)
                : null;
        this.items = items;
        this._enum = _enum != null && Iterables.size(_enum) > 0 ? ImmutableList.copyOf(_enum) : null;
        this.required = required != null && Iterables.size(required) > 0 ? ImmutableList.copyOf(required) : null;
        this.patternProperties = patternProperties != null && patternProperties.size() > 0
                ? ImmutableMap.copyOf(patternProperties)
                : null;
        this.definitions = definitions != null && definitions.size() > 0
                ? ImmutableMap.copyOf(definitions)
                : null;
        this.additionalProperties = type != null && type == Type.Object &&
                (this.properties != null || this.patternProperties != null) ? false : null;
    }

    public static Schema ref(String title)
    {
        return new Schema("#/definitions/" + titleToId(title), null, null, null, null, null, null, null, null, null, null, null);
    }

    public static String titleToId(final String title)
    {
        return title != null ? Joiner.on("-").join(Splitter.on(" ").split(title)).toLowerCase() : null;
    }

    public String get$ref()
    {
        return $ref;
    }

    public String getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }

    public String getDescription()
    {
        return description;
    }

    public String getType()
    {
        return type;
    }

    public String getFormat()
    {
        return format;
    }

    public Map<String, Schema> getProperties()
    {
        return properties;
    }

    public Schema getItems()
    {
        return items;
    }

    public List<String> get_enum()
    {
        return _enum;
    }

    public Map<String, Schema> getPatternProperties()
    {
        return patternProperties;
    }

    public List<Schema> getAnyOf()
    {
        return anyOf;
    }

    public Map<String, Schema> getDefinitions()
    {
        return definitions;
    }

    public Boolean getAdditionalProperties()
    {
        return additionalProperties;
    }

    public List<String> getRequired()
    {
        return required;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Schema that = (Schema) o;

        return Objects.equal(this.title, that.title) &&
                Objects.equal(this.description, that.description) &&
                Objects.equal(this.type, that.type) &&
                Objects.equal(this.properties, that.properties) &&
                Objects.equal(this.items, that.items) &&
                Objects.equal(this._enum, that._enum) &&
                Objects.equal(this.required, that.required);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(title, description, type, properties, items, _enum, required);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("title", title)
                .add("description", description)
                .add("type", type)
                .add("properties", properties)
                .add("items", items)
                .add("enum", _enum)
                .add("required", required)
                .toString();
    }

    public static final class Builder
    {
        private String id;
        private String title;
        private String description;
        private Type type;

        @TenantAware(SUPPRESS)
        private Map<String, Schema> properties = Maps.newLinkedHashMap();

        @TenantAware(SUPPRESS)
        private Map<String, Schema> patternProperties = Maps.newLinkedHashMap();
        private Schema items;
        private List<String> _enum = Lists.newArrayList();
        private List<String> required = Lists.newArrayList();

        @TenantAware(SUPPRESS)
        private Map<String, Schema> definitions = Maps.newTreeMap();
        private List<Schema> anyOf = Lists.newArrayList();

        private Builder() {}

        public Builder setId(String id)
        {
            this.id = id;
            return this;
        }

        public Builder setTitle(String title)
        {
            this.title = title;
            return this;
        }

        public Builder setDescription(String description)
        {
            this.description = description;
            return this;
        }

        public Builder setType(Type type)
        {
            this.type = type;
            return this;
        }

        public Builder setProperties(Map<String, Schema> properties)
        {
            this.properties = properties;
            return this;
        }

        public Builder addProperty(String propertyName, Schema propertySchema)
        {
            this.properties.put(propertyName, propertySchema);
            return this;
        }

        public Builder addPatternProperty(String pattern, Schema schema)
        {
            this.patternProperties.put(pattern, schema);
            return this;
        }

        public Builder setItems(Schema items)
        {
            this.items = items;
            return this;
        }

        public <T extends Enum<T>> Builder setEnum(Class<T> enumType)
        {
            this._enum = ImmutableList.copyOf(Iterables.transform(Arrays.asList(enumType.getEnumConstants()), new Function<T, String>()
            {
                @Override
                public String apply(final T input)
                {
                    return input.toString();
                }
            }));
            return this;
        }

        public Builder setRequired(List<String> required)
        {
            this.required = required;
            return this;
        }

        public Builder addRequired(String required)
        {
            this.required.add(required);
            return this;
        }

        public Builder addDefinition(Schema schema)
        {
            this.definitions.put(titleToId(schema.getTitle()), schema);
            return this;
        }

        public Builder addAnyOf(Schema schema)
        {
            this.anyOf.add(schema);
            return this;
        }

        public Schema build()
        {
            return new Schema(null, id, title, description, type, properties, items, _enum, required, patternProperties, definitions, anyOf);
        }
    }
}
