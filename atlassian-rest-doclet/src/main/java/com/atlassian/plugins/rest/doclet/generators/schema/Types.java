package com.atlassian.plugins.rest.doclet.generators.schema;

import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.plugins.rest.doclet.generators.schema.Schema.Type;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.annotate.JsonRawValue;
import org.joda.time.DateTime;

import java.lang.reflect.AnnotatedElement;
import java.net.URI;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.annotations.tenancy.TenancyScope.TENANTLESS;

public class Types {

    private Types() {}

    @TenantAware(TENANTLESS)
    private static final Map<Class<?>, Type> classToJsonType;

    static {
        ImmutableMap.Builder<Class<?>, Type> builder = ImmutableMap.<Class<?>, Type>builder()
                .put(String.class, Type.String)
                .put(Integer.class, Type.Integer)
                .put(Long.class, Type.Integer)
                .put(Number.class, Type.Number)
                .put(Boolean.class, Type.Boolean)
                .put(URI.class, Type.Uri)
                .put(Enum.class, Type.String)
                .put(Date.class, Type.String);
        try {
            builder.put(DateTime.class, Type.String);
        } catch (NoClassDefFoundError notFound) {
            // joda is not present, doesn't matter
        }

        classToJsonType = builder.build();
    }

    @TenantAware(TENANTLESS)
    private static final Map<String, Type> primitiveTypeToJsonType = ImmutableMap.<String, Type>builder()
            .put("boolean", Type.Boolean)
            .put("int", Type.Integer)
            .put("short", Type.Integer)
            .put("long", Type.Integer)
            .put("double", Type.Number)
            .put("float", Type.Number)
            .put("char", Type.String)
            .put("byte", Type.Integer)
            .build();

    public static Type resolveType(final RichClass modelClass, final AnnotatedElement containingField) {
        if (containingField != null && containingField.isAnnotationPresent(JsonRawValue.class)) {
            return Type.Any;
        } else if (isPrimitive(modelClass.getActualClass())) {
            return getPrimitiveType(modelClass.getActualClass()).get();
        } else if (isCollection(modelClass)) {
            return Type.Array;
        } else if (modelClass.getActualClass() == Object.class) {
            return Type.Any;
        } else {
            return Type.Object;
        }
    }

    public static boolean isPrimitive(Class<?> modelClass) {
        return getPrimitiveType(modelClass).isPresent();
    }

    private static Optional<Type> getPrimitiveType(Class<?> modelClass) {
        for (Map.Entry<Class<?>, Type> classToTypeName : classToJsonType.entrySet()) {
            if (classToTypeName.getKey().isAssignableFrom(modelClass)) {
                return Optional.of(classToTypeName.getValue());
            }
        }

        if (modelClass.isPrimitive()) {
            Type mapped = primitiveTypeToJsonType.get(modelClass.getSimpleName());
            return Optional.of(Objects.firstNonNull(mapped, Type.String));
        }

        return Optional.empty();
    }

    public static boolean isCollection(final RichClass type) {
        return type.getActualClass().isArray() || Iterable.class.isAssignableFrom(type.getActualClass()) && !type.getGenericTypes().isEmpty();
    }

    public static boolean isJDKClass(final Class<?> type) {
        String name = type.getCanonicalName() != null ? type.getCanonicalName() : type.getName();
        return name.startsWith("java");
    }
}
