package com.atlassian.plugins.rest.doclet.generators.resourcedoc;

import com.sun.jersey.api.model.AbstractResource;
import com.sun.jersey.api.model.AbstractResourceMethod;
import com.sun.jersey.server.wadl.WadlGeneratorImpl;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;
import com.sun.research.ws.wadl.Resource;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.POST;
import java.lang.annotation.Annotation;

public class AtlassianWadlGeneratorResourceDocSupportTest {
    @Test
    public void testGeneratedDocUrlDoesNotContainNoneVersion() throws Exception {
        final AtlassianWadlGeneratorResourceDocSupport generator = new AtlassianWadlGeneratorResourceDocSupport(new WadlGeneratorImpl(), new ResourceDocType());
        generator.setResourceDocStream(generator.getClass().getClassLoader().getResourceAsStream("sampleresourcedoc.xml"));
        generator.init();

        AbstractResource r = new AbstractResource(MockResource.class);
        r.getResourceMethods().add(new AbstractResourceMethod(r, MockResource.class.getMethods()[0], Void.class, Void.class, "POST", new Annotation[]{}));
        final Resource documentedResource = generator.createResource(r, "doSomething");
        Assert.assertEquals("vr/doSomething", documentedResource.getPath());
    }

    private class MockResource {
        @POST
        public void postMethod() {}
    }

}
