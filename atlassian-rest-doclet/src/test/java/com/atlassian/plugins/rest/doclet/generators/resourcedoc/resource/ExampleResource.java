package com.atlassian.plugins.rest.doclet.generators.resourcedoc.resource;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.ArrayBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.SimpleBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.SimpleListBean;
import com.atlassian.rest.annotation.RequestType;
import com.atlassian.rest.annotation.ResponseType;

import static com.atlassian.rest.annotation.ResponseType.StatusType.CLIENT_ERROR;

@ResponseType(statusType = CLIENT_ERROR, value = SimpleListBean.class)
public class ExampleResource
{
    @RequestType (SimpleBean.class)
    @ResponseType (ArrayBean.class)
    @Path ("{id}")
    @POST
    public SimpleListBean createSimple(@PathParam ("id") String id, ArrayBean simpleBean)
    {
        return null;
    }

    @ResponseType (ArrayBean.class)
    @Path ("{id}")
    @POST
    public Response createSimpleWithAutoRequestType(@PathParam ("id") String id, @QueryParam ("a") String a, SimpleBean simpleBean, @QueryParam ("b") String b)
    {
        return null;
    }

    @ResponseType(Void.class)
    public Response voidResponseTypeAnnotated() {
        return null;
    }

    public void voidActualReturnType() {
        return;
    }

    public SimpleBean methodWithActualReturnType()
    {
        return null;
    }

    public Response methodWithResponseReturnType()
    {
        return null;
    }
}
