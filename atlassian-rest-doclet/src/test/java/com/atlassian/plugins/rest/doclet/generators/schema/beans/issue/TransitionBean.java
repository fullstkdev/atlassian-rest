package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.SelfExpanding;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @since v4.2
 */
@SuppressWarnings ({ "FieldCanBeLocal", "UnusedDeclaration" })
@XmlRootElement
@JsonIgnoreProperties (ignoreUnknown = true)
public class TransitionBean
{
    @XmlElement
    private String id;

    @XmlElement
    private String name;

    @XmlElement
    private StatusJsonBean to;

    @XmlElement
    private Map<String, FieldMetaBean> fields;

    @XmlTransient
    private Object fieldsBuilder;

    @XmlTransient
    @Expandable ("fields")
    private SelfExpanding fieldsExpander = new SelfExpanding()
    {
        public void expand()
        {
            fields = null;
        }
    };

    @XmlElement
    private String expand;
}
