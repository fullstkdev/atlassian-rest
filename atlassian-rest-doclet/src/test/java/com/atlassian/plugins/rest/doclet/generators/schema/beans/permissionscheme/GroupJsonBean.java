package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import java.net.URI;

/**
 * @since v5.0
 */
public class GroupJsonBean
{
    @JsonProperty
    private String name;

    @JsonProperty
    private URI self;

    public GroupJsonBean()
    {
    }

    public GroupJsonBean(String name, URI self)
    {
        this.name = name;
        this.self = self;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public URI getSelf()
    {
        return self;
    }

    @JsonIgnore
    public void setSelf(URI self)
    {
        this.self = self;
    }
}

