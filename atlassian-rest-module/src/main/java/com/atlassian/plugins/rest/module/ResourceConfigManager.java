package com.atlassian.plugins.rest.module;

import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugins.rest.common.expand.AdditionalExpandsProvider;
import com.atlassian.plugins.rest.common.expand.SelfExpandingExpander;
import com.atlassian.plugins.rest.common.expand.interceptor.ExpandInterceptor;
import com.atlassian.plugins.rest.common.expand.resolver.ChainingEntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.CollectionEntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.ExpandConstraintEntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.IdentityEntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.ListWrapperEntityExpanderResolver;
import com.atlassian.plugins.rest.common.filter.ExtensionJerseyFilter;
import com.atlassian.plugins.rest.common.interceptor.impl.InterceptorChainBuilderProvider;
import com.atlassian.plugins.rest.common.security.jersey.AntiSniffingResponseFilter;
import com.atlassian.plugins.rest.module.expand.resolver.PluginEntityExpanderResolver;
import com.atlassian.plugins.rest.module.filter.AcceptHeaderJerseyMvcFilter;
import com.atlassian.plugins.rest.module.filter.AcceptLanguageFilter;
import com.atlassian.plugins.rest.module.filter.CorsAcceptOptionsPreflightFilter;
import com.atlassian.plugins.rest.module.json.JsonWithPaddingResponseFilter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.sun.jersey.api.core.DefaultResourceConfig;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;
import com.sun.jersey.spi.container.ResourceMethodDispatchProvider;
import com.sun.jersey.spi.inject.InjectableProvider;
import com.sun.jersey.spi.template.TemplateProcessor;

import org.codehaus.jackson.map.Module;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import javax.ws.rs.ext.MessageBodyReader;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugins.rest.module.json.JsonWithPaddingResponseFilter.ATLASSIAN_ALLOW_JSONP;
import static com.google.common.collect.Lists.newArrayList;

public class ResourceConfigManager {
    private final OsgiServiceAccessor<ResourceFilterFactory> resourceFilterFactories;
    private final OsgiServiceAccessor<InjectableProvider> injectableProviders;
    private final OsgiServiceAccessor<TemplateProcessor> templateProcessors;
    private final OsgiServiceAccessor<MessageBodyReader> messageBodyReaders;
    private final OsgiServiceAccessor<ResourceMethodDispatchProvider> dispatchProviders;
    private final OsgiServiceAccessor<Module> modules;
    private final OsgiServiceAccessor<AdditionalExpandsProvider> additionalExpandsProvider;
    private final ContainerManagedPlugin plugin;
    private final Bundle bundle;

    public ResourceConfigManager(ContainerManagedPlugin plugin, Bundle bundle) {
        this.plugin = plugin;
        this.bundle = bundle;
        BundleContext bundleContext = bundle.getBundleContext();

        // looking up resource filters
        resourceFilterFactories = new OsgiServiceAccessor<ResourceFilterFactory>(ResourceFilterFactory.class, bundleContext, new OsgiFactory<ResourceFilterFactory>() {
            public ResourceFilterFactory getInstance(BundleContext bundleContext, ServiceReference serviceReference) {
                return new OsgiServiceReferenceResourceFilterFactory(bundleContext, serviceReference);
            }
        });

        // looking up injectable providers
        injectableProviders = new OsgiServiceAccessor<InjectableProvider>(InjectableProvider.class, bundleContext, new OsgiFactory<InjectableProvider>() {
            public InjectableProvider getInstance(BundleContext bundleContext, ServiceReference serviceReference) {
                return (InjectableProvider) bundleContext.getService(serviceReference);
            }
        });

        templateProcessors = new OsgiServiceAccessor<TemplateProcessor>(TemplateProcessor.class, bundleContext, new OsgiFactory<TemplateProcessor>() {
            public TemplateProcessor getInstance(BundleContext bundleContext, ServiceReference serviceReference) {
                return (TemplateProcessor) bundleContext.getService(serviceReference);
            }
        });

        messageBodyReaders = new OsgiServiceAccessor<MessageBodyReader>(MessageBodyReader.class, bundleContext, new OsgiFactory<MessageBodyReader>() {
            public MessageBodyReader getInstance(final BundleContext bundleContext, final ServiceReference serviceReference) {
                return (MessageBodyReader) bundleContext.getService(serviceReference);
            }
        });

        dispatchProviders = new OsgiServiceAccessor<ResourceMethodDispatchProvider>(ResourceMethodDispatchProvider.class, bundleContext, new OsgiFactory<ResourceMethodDispatchProvider>() {
            public ResourceMethodDispatchProvider getInstance(final BundleContext bundleContext, final ServiceReference serviceReference) {
                return (ResourceMethodDispatchProvider) bundleContext.getService(serviceReference);
            }
        });

        modules = new OsgiServiceAccessor<Module>(Module.class, bundleContext, new OsgiFactory<Module>() {
            public Module getInstance(final BundleContext bundleContext, final ServiceReference serviceReference) {
                return (Module) bundleContext.getService(serviceReference);
            }
        });

        additionalExpandsProvider = new OsgiServiceAccessor<>(AdditionalExpandsProvider.class, bundleContext, (bundleContext1, serviceReference) -> (AdditionalExpandsProvider) bundleContext1.getService(serviceReference));
    }

    public DefaultResourceConfig createResourceConfig(Map<String, Object> props, String[] excludes, Set<String> packages) {
        // get the excludes parameter
        final Collection<String> excludesCollection = excludes != null ? Arrays.asList(excludes) : Collections.<String>emptyList();

        final EntityExpanderResolver expanderResolver = new ChainingEntityExpanderResolver(Lists.<EntityExpanderResolver>newArrayList(
                new PluginEntityExpanderResolver(plugin),
                new CollectionEntityExpanderResolver(),
                new ListWrapperEntityExpanderResolver(),
                new ExpandConstraintEntityExpanderResolver(),
                new SelfExpandingExpander.Resolver(),
                new IdentityEntityExpanderResolver()
        ));

        final Collection<Object> providers = Lists.newLinkedList();
        providers.addAll(injectableProviders.get());
        providers.addAll(templateProcessors.get());
        providers.addAll(messageBodyReaders.get());
        providers.addAll(dispatchProviders.get());

        providers.add(new InterceptorChainBuilderProvider(plugin, new ExpandInterceptor(expanderResolver, additionalExpandsProvider.get())));

        List<ContainerRequestFilter> containerRequestFilters = newArrayList(
                new ExtensionJerseyFilter(excludesCollection),
                new AcceptHeaderJerseyMvcFilter(),
                new AcceptLanguageFilter(),
                new CorsAcceptOptionsPreflightFilter()
        );

        ImmutableList.Builder containerResponseFilters =
                new ImmutableList.Builder<ContainerResponseFilter>();
        containerResponseFilters.add(new AntiSniffingResponseFilter());

        if ("true".equals(System.getProperty(ATLASSIAN_ALLOW_JSONP))) {
            containerResponseFilters.add(new JsonWithPaddingResponseFilter());
        }

        final OsgiResourceConfig osgiResourceConfig = new OsgiResourceConfig(bundle, packages,
                containerRequestFilters,
                containerResponseFilters.build(),
                resourceFilterFactories.get(),
                modules.get(),
                providers);

        return osgiResourceConfig;
    }

    public void destroy() {
        resourceFilterFactories.release();
        injectableProviders.release();
        templateProcessors.release();
        messageBodyReaders.release();
        dispatchProviders.release();
        modules.release();
        additionalExpandsProvider.release();
    }


}
