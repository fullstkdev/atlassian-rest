package com.atlassian.plugins.rest.module;

import com.google.common.base.Preconditions;
import com.sun.jersey.api.core.DefaultResourceConfig;
import org.apache.commons.lang.StringUtils;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class RestApiContext {
    public static final String SLASH = "/";

    public final static String LATEST = SLASH + "latest";

    public final static String ANY_PATH_PATTERN = "/*";

    private final String restContext;
    private final String apiPath;
    private final ApiVersion version;
    private final Set<String> packages;

    private final AtomicReference<DefaultResourceConfig> osgiResourceConfig = new AtomicReference<>();

    public RestApiContext(String restContext, String apiContext, ApiVersion version, Set<String> packages) {
        this.restContext = prependSlash(Preconditions.checkNotNull(restContext));
        this.apiPath = prependSlash(Preconditions.checkNotNull(apiContext));
        this.version = Preconditions.checkNotNull(version);
        this.packages = Preconditions.checkNotNull(packages);
    }

    /**
     * @return the REST context, always starts with "/".
     */
    public String getRestContext() {
        return restContext;
    }

    /**
     * @return the API path, always starts with "/"
     */
    public String getApiPath() {
        return apiPath;
    }

    /**
     * @return the API version
     */
    public ApiVersion getVersion() {
        return version;
    }

    /**
     * @return String representing /restContext/apiPath/version, always starts with "/"
     */
    public String getPathToVersion() {
        // if you say that you want to go versionless then you get the latest version
        // (this is a point of indirection where we could potentially choose a version other than latest)
        return getPathToVersion(version);
    }

    /**
     * @return String representing /restContext/apiPath/"latest", always starts with "/"
     */
    public String getPathToLatest() {
        return getPathToVersion(LATEST);
    }

    /**
     * @param version ApiVersion such as "1.2.3" or "none"
     * @return String representing /restContext/apiPath/version, always starts with "/"
     */
    public String getPathToVersion(String version) {
        return restContext + getContextlessPathToVersion(version);
    }

    /**
     * @param version ApiVersion such as "1.2.3" or "none"
     * @return String representing /restContext/apiPath/version, always starts with "/"
     */
    private String getPathToVersion(ApiVersion version) {
        return restContext + getContextlessPathToVersion(version);
    }

    /**
     * @return String representing /apiPath/version, always starts with "/"
     */
    public String getContextlessPathToVersion() {
        return getContextlessPathToVersion(version);
    }

    /**
     * @param version ApiVersion such as "1.2.3" or "none"
     * @return String representing /apiPath/version, always starts with "/"
     */
    private String getContextlessPathToVersion(String version) {
        return ApiVersion.isNone(version)
                ? apiPath
                : apiPath + prependSlash(version);
    }

    /**
     * Handles the version="none" case more efficiently than the overload that takes a String.
     *
     * @param version ApiVersion such as "1.2.3" or "none"
     * @return String representing /apiPath/version, always starts with "/"
     */
    private String getContextlessPathToVersion(ApiVersion version) {
        return version.isNone()
                ? apiPath
                : apiPath + prependSlash(version.toString());
    }

    private String prependSlash(String path) {
        return StringUtils.startsWith(path, SLASH) ? path : SLASH + path;
    }

    public Set<String> getPackages() {
        return packages;
    }

    public void setConfig(DefaultResourceConfig config) {
        osgiResourceConfig.set(config);
    }

    public Optional<DefaultResourceConfig> getConfig() {
        return osgiResourceConfig.get() != null
                ? Optional.of(osgiResourceConfig.get())
                : Optional.empty();
    }

    public void disabled() {
        osgiResourceConfig.set(null);
    }
}
