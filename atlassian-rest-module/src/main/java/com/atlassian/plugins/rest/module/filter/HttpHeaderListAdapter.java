package com.atlassian.plugins.rest.module.filter;

import java.text.ParseException;

import com.sun.jersey.core.header.reader.HttpHeaderReader;

// Direct copy from Jersey sources, made because source class is
// package private. DO NOT CHANGE!
/* package */ class HttpHeaderListAdapter extends HttpHeaderReader {
    private HttpHeaderReader reader;

    boolean isTerminated;

    public HttpHeaderListAdapter(HttpHeaderReader reader) {
        this.reader = reader;
    }

    public void reset() {
        isTerminated = false;
    }


    @Override
    public boolean hasNext() {
        if (isTerminated)
            return false;

        if (reader.hasNext()) {
            if (reader.hasNextSeparator(',', true)) {
                isTerminated = true;
                return false;
            } else
                return true;
        }

        return false;
    }

    @Override
    public boolean hasNextSeparator(char separator, boolean skipWhiteSpace) {
        if (isTerminated)
            return false;

        if (reader.hasNextSeparator(',', skipWhiteSpace)) {
            isTerminated = true;
            return false;
        } else
            return reader.hasNextSeparator(separator, skipWhiteSpace);
    }

    @Override
    public Event next() throws ParseException {
        return next(true);
    }

    @Override
    public HttpHeaderReader.Event next(boolean skipWhiteSpace) throws ParseException {
        return next(skipWhiteSpace, false);
    }

    @Override
    public HttpHeaderReader.Event next(boolean skipWhiteSpace, boolean preserveBackslash) throws ParseException {
        if (isTerminated)
            throw new ParseException("End of header", getIndex());

        if (reader.hasNextSeparator(',', skipWhiteSpace)) {
            isTerminated = true;
            throw new ParseException("End of header", getIndex());
        }

        return reader.next(skipWhiteSpace, preserveBackslash);
    }

    @Override
    public String nextSeparatedString(char startSeparator, char endSeparator) throws ParseException {
        if (isTerminated)
            throw new ParseException("End of header", getIndex());

        if (reader.hasNextSeparator(',', true)) {
            isTerminated = true;
            throw new ParseException("End of header", getIndex());
        }

        return reader.nextSeparatedString(startSeparator, endSeparator);
    }

    @Override
    public HttpHeaderReader.Event getEvent() {
        return reader.getEvent();
    }

    @Override
    public String getEventValue() {
        return reader.getEventValue();
    }

    @Override
    public String getRemainder() {
        return reader.getRemainder();
    }

    @Override
    public int getIndex() {
        return reader.getIndex();
    }
}
