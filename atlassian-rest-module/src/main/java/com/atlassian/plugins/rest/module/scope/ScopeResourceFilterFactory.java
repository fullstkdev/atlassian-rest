package com.atlassian.plugins.rest.module.scope;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugins.rest.module.RestModuleDescriptor;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ext.Provider;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * Returns scope filter based on request's rest resource
 *
 * @since 3.2
 */
@Provider
public class ScopeResourceFilterFactory implements ResourceFilterFactory {
    private static final Logger log = LoggerFactory.getLogger(ScopeResourceFilterFactory.class);

    private final PluginModuleTracker<Object, RestModuleDescriptor> pluginModuleTracker;
    private final ScopeManager scopeManager;

    public ScopeResourceFilterFactory(
            PluginAccessor pluginAccessor,
            PluginEventManager pluginEventManager,
            ScopeManager scopeManager) {
        this.pluginModuleTracker = new DefaultPluginModuleTracker<>(pluginAccessor, pluginEventManager, RestModuleDescriptor.class);
        this.scopeManager = scopeManager;
    }

    /**
     * Returns scope resource filter if given method belongs to plugin where scope is enabled
     */
    public List<ResourceFilter> create(final AbstractMethod method) {
        final Class clazz = method.getResource().getResourceClass();
        final Iterable<RestModuleDescriptor> moduleDescriptors = pluginModuleTracker.getModuleDescriptors();
        final Stream<RestModuleDescriptor> stream = StreamSupport.stream(moduleDescriptors.spliterator(), false);

        //for non initalized plugins rest api context won't be present
        final Predicate<RestModuleDescriptor> restApiContextIsPresent = m -> m.getRestApiContext() !=null;

        //if config for current MD is not present it might be that it wasn't initialized yet
        final Predicate<RestModuleDescriptor> configMatches = m -> m.getRestApiContext().getConfig().map(
                c -> c.getClasses().contains(clazz)).orElse(false);

        //composite predicate for initialized MDs and match resource class
        final Predicate<RestModuleDescriptor> moduleDescriptorPredicate = restApiContextIsPresent.and(configMatches);

        //O(N) on all rest MDs registered. Given N is a constant no caching introduced, yet
        final Optional<RestModuleDescriptor> descriptor = stream.filter(moduleDescriptorPredicate).findFirst();

        return descriptor.isPresent()
                ? singletonList(new ScopeResourceFilter(scopeManager, descriptor.get()))
                : emptyList();
    }
}
