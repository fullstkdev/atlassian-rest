/**
 * Classes responsible for product scope checks
 *
 * @see com.atlassian.plugin.ScopeAware
 * @see com.atlassian.plugin.scope.ScopeManager
 * @since 3.2
 */
package com.atlassian.plugins.rest.module.scope;