package com.atlassian.plugins.rest.module;

import com.atlassian.plugins.rest.common.version.ApiVersion;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class TestRestApiContext {
    @Test
    public void fullyParametrisedRestApiContextProducesCorrectUrl() {
        RestApiContext context = new RestApiContext("restContext", "apiContext", new com.atlassian.plugins.rest.module.ApiVersion("1.2.3"), Collections.<String>emptySet());
        assertThat(context.getPathToVersion(), is("/restContext/apiContext/1.2.3"));
    }

    @Test
    public void restApiContextWithoutVersionProducesLatestVersionUrl() {
        RestApiContext context = new RestApiContext("restContext", "apiContext", new com.atlassian.plugins.rest.module.ApiVersion(ApiVersion.NONE_STRING), Collections.<String>emptySet());
        assertThat(context.getPathToVersion(), is("/restContext/apiContext"));
    }

    @Test
    public void contextlessPathIsCorrect() {
        RestApiContext restApiContext = new RestApiContext("restContext", "apiContext", new com.atlassian.plugins.rest.module.ApiVersion("1.2.3"), Collections.<String>emptySet());
        assertThat(restApiContext.getContextlessPathToVersion(), is(restApiContext.getApiPath() + RestApiContext.SLASH + restApiContext.getVersion()));
    }
}
