package com.atlassian.plugins.rest.module.filter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import java.util.List;
import java.util.Locale;

import javax.ws.rs.core.HttpHeaders;

import org.junit.Test;

import com.sun.jersey.core.header.InBoundHeaders;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.WebApplication;

public class AcceptLanguageFilterTest {
    private static final ContainerRequest createRequestWithLanguages(String langs) {
        WebApplication wa = mock(WebApplication.class);
        InBoundHeaders headers = new InBoundHeaders();
        headers.add(HttpHeaders.ACCEPT_LANGUAGE, langs);
        return new ContainerRequest(wa, null, null, null, headers, null);
    }

    private static final List<Locale> parse(String langs) {
        AcceptLanguageFilter f = new AcceptLanguageFilter();
        ContainerRequest req = createRequestWithLanguages(langs);
        ContainerRequest r = f.filter(req);
        return r.getAcceptableLanguages();
    }

    @Test
    public void testLanguageTag() {
        List<Locale> l = parse("en-US");
        assertEquals("size", 1, l.size());
        assertEquals("content#0", Locale.forLanguageTag("en-US"), l.get(0));
    }

    @Test
    public void testUNM49Tag() {
        List<Locale> l = parse("es-419");
        assertEquals("size", 1, l.size());
        assertEquals("content#0", Locale.forLanguageTag("es-419"), l.get(0));
    }

    @Test
    public void testAcceptableLanguageTag() {
        List<Locale> l = parse("en-US;q=0.123");
        assertEquals("size", 1, l.size());
        assertEquals("content#0", Locale.forLanguageTag("en-US"), l.get(0));
    }

    @Test
    public void testAcceptableLanguageTagList() {
        List<Locale> l = parse("en-US;q=0.123, fr;q=0.2, en;q=0.3, *;q=0.01");
        assertEquals("size", 4, l.size());
        assertEquals("content#0", Locale.forLanguageTag("en"), l.get(0));
        assertEquals("content#1", Locale.forLanguageTag("fr"), l.get(1));
        assertEquals("content#2", Locale.forLanguageTag("en-US"), l.get(2));
        assertEquals("content#3", new Locale("*"), l.get(3));
    }

    @Test
    public void testHeadersChange() {
        ContainerRequest r = createRequestWithLanguages("en-US");
        AcceptLanguageFilter f = new AcceptLanguageFilter();
        r = f.filter(r);

        // Parse & cache langs
        r.getAcceptableLanguages();

        // Change headers value
        InBoundHeaders headers = new InBoundHeaders();
        headers.add(HttpHeaders.ACCEPT_LANGUAGE, "fr");

        // Parse again
        r.setHeaders(headers);
        List<Locale> l = r.getAcceptableLanguages();
        assertEquals("size", 1, l.size());
        assertEquals("content#0", Locale.forLanguageTag("fr"), l.get(0));
    }
}
