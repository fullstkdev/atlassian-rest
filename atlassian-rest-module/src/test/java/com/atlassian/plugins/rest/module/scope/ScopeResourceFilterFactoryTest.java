package com.atlassian.plugins.rest.module.scope;


import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.plugins.rest.module.RestModuleDescriptor;
import com.atlassian.plugins.rest.module.servlet.RestServletModuleManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.sun.jersey.api.core.DefaultResourceConfig;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.api.model.AbstractResource;
import com.sun.jersey.api.model.AbstractResourceMethod;
import com.sun.jersey.api.model.PathValue;
import com.sun.jersey.spi.container.ResourceFilter;
import org.dom4j.Element;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.HttpMethod;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScopeResourceFilterFactoryTest {

    @Mock
    PluginAccessor pluginAccessor;

    @Mock
    PluginEventManager pluginEventManager;

    @Mock
    ScopeManager scopeManager;

    @Mock
    ModuleFactory moduleFactory;

    @Mock
    RestServletModuleManager restServletModuleManager;

    @Mock
    OsgiPlugin plugin;

    @Mock
    Element element;

    static class MappedClass {
        public void get() {
        }

        static Method getMethod() {
            try {
                return MappedClass.class.getMethod("get");
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
    }

    static class UnmappedClass {
    }


    final AbstractMethod httpMethod = new AbstractResourceMethod(
            new AbstractResource(MappedClass.class, new PathValue("/")),
            MappedClass.getMethod(),
            Void.class,
            Void.class,
            HttpMethod.GET,
            new Annotation[]{});

    @Before
    public void setUp() {
        when(element.attributeValue("path")).thenReturn("/rest");
        when(element.attributeValue("version")).thenReturn("1.0");
        when(element.elements("package")).thenReturn(ImmutableList.of());
    }

    @Test
    public void verifyEmptyFilterIfRestApiNotInitialized() throws Exception {
        final RestModuleDescriptor descriptor = new RestModuleDescriptor(moduleFactory, restServletModuleManager, "/rest");
        descriptor.init(plugin, element);

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(RestModuleDescriptor.class)).thenReturn(ImmutableList.of(descriptor));

        final ScopeResourceFilterFactory factory = new ScopeResourceFilterFactory(pluginAccessor, pluginEventManager, scopeManager);

        final List<ResourceFilter> filters = factory.create(httpMethod);

        assertTrue(filters.isEmpty());
    }

    @Test
    public void verifyEmptyFilterIfNoClassMatch() throws Exception {
        final RestModuleDescriptor descriptor = new RestModuleDescriptor(moduleFactory, restServletModuleManager, "/rest");
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(RestModuleDescriptor.class)).thenReturn(ImmutableList.of(descriptor));
        descriptor.init(plugin, element);

        final DefaultResourceConfig config = new DefaultResourceConfig(ImmutableSet.of(UnmappedClass.class));

        descriptor.getRestApiContext().setConfig(config);

        final ScopeResourceFilterFactory factory = new ScopeResourceFilterFactory(pluginAccessor, pluginEventManager, scopeManager);

        final List<ResourceFilter> filters = factory.create(httpMethod);

        assertTrue(filters.isEmpty());
    }

    @Test
    public void verifyNonEmptyFilterIfRestApiInitializedAndClassMatch() throws Exception {
        final RestModuleDescriptor descriptor = new RestModuleDescriptor(moduleFactory, restServletModuleManager, "/rest");
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(RestModuleDescriptor.class)).thenReturn(ImmutableList.of(descriptor));
        descriptor.init(plugin, element);

        final DefaultResourceConfig config = new DefaultResourceConfig(ImmutableSet.of(MappedClass.class));

        descriptor.getRestApiContext().setConfig(config);

        final ScopeResourceFilterFactory factory = new ScopeResourceFilterFactory(pluginAccessor, pluginEventManager, scopeManager);

        final List<ResourceFilter> filters = factory.create(httpMethod);

        assertThat(filters, CoreMatchers.hasItems(CoreMatchers.instanceOf(ScopeResourceFilter.class)));
    }
}