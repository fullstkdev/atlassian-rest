package com.atlassian.plugins.rest.autowiring;

import org.apache.commons.lang.Validate;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/autowired")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class AutowiredResource {
    private final SomeService someService;

    public AutowiredResource(SomeService someService) {
        Validate.notNull(someService);
        this.someService = someService;
    }

    @GET
    public SomeService getResource() {
        return someService;
    }
}
