package com.atlassian.plugins.rest.cors;

import com.atlassian.plugins.rest.common.security.CorsAllowed;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

/**
 */
@Path("/cors")
@AnonymousAllowed
public class CorsProtectedResource {
    @GET
    @Path("none")
    public String getWithNoHeaders() {
        return "Request succeeded";
    }

    @GET
    @CorsAllowed
    public String getWithHeaders() {
        return "Request succeeded";
    }

    @POST
    @Path("none")
    public String postWithNoHeaders() {
        return "Request succeeded";
    }

    @POST
    @CorsAllowed
    public String postWithHeaders() {
        return "Request succeeded";
    }

    @PUT
    @Path("none")
    public String putWithNoHeaders() {
        return "Request succeeded";
    }

    @PUT
    @CorsAllowed
    public String putWithHeaders() {
        return "Request succeeded";
    }
}
