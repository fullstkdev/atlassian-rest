package com.atlassian.plugins.rest.expand;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Resource for testing expand functionality.
 */
@Path("/expand")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class ExpandResource {
    @GET
    public Expand get() {
        return new Expand();
    }
}
