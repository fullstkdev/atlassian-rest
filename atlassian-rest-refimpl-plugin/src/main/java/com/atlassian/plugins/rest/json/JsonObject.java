package com.atlassian.plugins.rest.json;

import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@XmlRootElement
public class JsonObject {
    @XmlAttribute
    private int anInteger = 1;

    @XmlAttribute
    private long aLong = 2L;

    @XmlElementWrapper
    @XmlElement(name = "item")
    private Collection<JsonItem> nullCollection = null;

    @XmlElementWrapper
    @XmlElement
    private Collection<JsonItem> emptyCollection = Lists.newArrayList();

    @XmlElementWrapper
    @XmlElement(name = "item")
    private Collection<JsonItem> singletonCollection = Lists.newArrayList(new JsonItem("item1"));

    @XmlElementWrapper
    private Map<String, String> singletonMap = Collections.singletonMap("foo", "bar");

    @XmlRootElement
    public static class JsonItem {
        @XmlAttribute
        private String name;

        public JsonItem() {
        }

        public JsonItem(String name) {
            this.name = name;
        }
    }
}
