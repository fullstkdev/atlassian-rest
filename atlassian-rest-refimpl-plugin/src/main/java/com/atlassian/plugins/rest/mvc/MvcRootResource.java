package com.atlassian.plugins.rest.mvc;

import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Path;

@Path("/")
@Singleton
public class MvcRootResource {
}
