package com.atlassian.plugins.rest.scope.explicit;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/get")
@AnonymousAllowed
public class ExplicitScopeResource {
    @GET
    @Produces("text/plain")
    public String get() {
        return "exp";
    }
}
