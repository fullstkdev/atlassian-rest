package com.atlassian.plugins.rest.scope.implicit;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/get")
@AnonymousAllowed
public class ImplicitScopeResource {
    @GET
    @Produces("text/plain")
    public String get() {
        return "imp";
    }
}
