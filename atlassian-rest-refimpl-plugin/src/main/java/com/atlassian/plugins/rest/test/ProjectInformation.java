package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.Expander;
import com.google.common.base.Preconditions;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Expander(ProjectInformationExpander.class)
public class ProjectInformation {
    @XmlAttribute
    private String expand;

    @XmlElement
    private final Link link;

    @XmlElement
    private String longName;

    @XmlElement
    private String longDescription;

    @XmlElement
    @Expandable("specification")
    private ProjectSpecification specification;

    private ProjectInformation() {
        this.link = null;
    }

    public ProjectInformation(Link link) {
        this.link = Preconditions.checkNotNull(link);
    }

    public static ProjectInformation getInformation(UriInfo uriInfo) {
        final ProjectInformation information = new ProjectInformation(Link.self(uriInfo.getAbsolutePathBuilder().path("information").build()));
        information.setSpecification(ProjectSpecification.getSpecification(uriInfo));
        return information;
    }

    public Link getLink() {
        return link;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public ProjectSpecification getSpecification() {
        return specification;
    }

    public void setSpecification(ProjectSpecification specification) {
        this.specification = specification;
    }
}
