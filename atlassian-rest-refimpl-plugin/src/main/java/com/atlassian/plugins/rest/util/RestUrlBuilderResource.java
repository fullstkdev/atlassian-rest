package com.atlassian.plugins.rest.util;

import com.atlassian.plugins.rest.common.util.RestUrlBuilder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/restUrlBuilder")
public class RestUrlBuilderResource {
    private final RestUrlBuilder urlBuilder;

    public RestUrlBuilderResource(RestUrlBuilder urlBuilder) {
        this.urlBuilder = urlBuilder;
    }

    @GET
    @Path("/dummyResource")
    @Produces("text/plain")
    public String getUrlForDummyResource() throws URISyntaxException {
        return urlBuilder.getUrlFor(new URI("http://atlassian.com:1234/foo"), DummyResource.class).subResource().toString();
    }
}
