package it.com.atlassian.rest.authorisation;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

public class AuthorisationTest {
    private WebResource authenticatedWebResource;
    private WebResource anonymousWebResource;

    @Before
    public void setUp() {
        authenticatedWebResource = WebResourceFactory.authenticated().path("/");
        anonymousWebResource = WebResourceFactory.anonymous().path("/");
    }

    @Test
    public void testCannotAccessResourceAnonymously() {
        try {
            anonymousWebResource.get(String.class);
            fail();
        } catch (UniformInterfaceException e) {
            assertEquals(Response.Status.UNAUTHORIZED, e.getResponse().getResponseStatus());
        }
    }

    @Test
    public void testCanAccessResourceWhenAuthenticated() {
        assertEquals("This is the root resource", authenticatedWebResource.get(String.class));
    }
}
