package it.com.atlassian.rest.error;

import com.atlassian.plugins.rest.common.Status;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

public class ErrorPagesTest {
    private WebResource webResource;

    @Before
    public void setUp() {
        webResource = WebResourceFactory.authenticated();
    }

    @Test
    public void testNotFoundReturns404() {
        try {
            webResource.path("somepaththatdoesntexist").get(Object.class);
            fail("This should have thrown an exception as the page doesn't exists");
        } catch (UniformInterfaceException e) {
            assertEquals(Response.Status.NOT_FOUND, e.getResponse().getResponseStatus());
            assertEquals(Response.Status.NOT_FOUND.getStatusCode(), e.getResponse().getEntity(Status.class).getCode());
        }
    }
}
