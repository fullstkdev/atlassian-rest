package it.com.atlassian.rest.error;

import javax.ws.rs.core.MediaType;

import com.atlassian.rest.jersey.client.WebResourceFactory;

import com.sun.jersey.api.client.ClientResponse;

import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import net.sf.json.JSONObject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class UncaughtErrorTest {
    private static final String MESSAGE = "my error detail string";

    @Test
    public void testUncaughtErrorResponseIsReturnedInXMLWhenXMLIsRequested() {
        final ClientResponse response = WebResourceFactory.anonymous()
                .path("errors/uncaughtInternalError")
                .queryParam("message", MESSAGE)
                .accept("application/xml")
                .get(ClientResponse.class);
        assertEquals(500, response.getStatus());
        assertEquals("application/xml", response.getType().toString());

        String entity = response.getEntity(String.class);

        assertThat(entity,
                JUnitMatchers.containsString("<status-code>500</status-code>"));
        assertThat(entity,
                JUnitMatchers.containsString("<message>" + MESSAGE + "</message>"));
    }

    @Test
    public void testUncaughtErrorResponseIsReturnedInJSONWhenJSONIsRequested() {
        final ClientResponse response = WebResourceFactory.anonymous()
                .path("errors/uncaughtInternalError")
                .queryParam("message", MESSAGE)
                .accept("application/json")
                .get(ClientResponse.class);
        assertEquals(500, response.getStatus());
        assertEquals("application/json", response.getType().toString());

        JSONObject obj = JSONObject.fromObject(response.getEntity(String.class));

        assertEquals(MESSAGE, obj.getString("message"));
        assertEquals("500", obj.getString("status-code"));

        assertNotNull(obj.getString("stack-trace"));
    }

    @Test
    public void testUncaughtErrorResponseIsReturnedInTextWhenTextIsRequested() {
        final ClientResponse response = WebResourceFactory.anonymous()
                .path("errors/uncaughtInternalError")
                .queryParam("message", MESSAGE)
                .accept("text/plain")
                .get(ClientResponse.class);
        assertEquals(500, response.getStatus());

        assertEquals(response.getType(),
                MediaType.valueOf("text/plain; charset=utf-8"));

        String entity = response.getEntity(String.class);
        assertThat(entity,
                JUnitMatchers.containsString(MESSAGE));
        assertThat(entity,
                JUnitMatchers.containsString(IllegalStateException.class.getName()));
    }
}
