package it.com.atlassian.rest.header;

import com.atlassian.plugins.rest.common.security.jersey.AntiSniffingResponseFilter;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class AntiSniffingHeaderTest {

    private WebResource authenticatedWebResource;
    private WebResource anonymousWebResource;

    @Before
    public void setUp() {
        authenticatedWebResource = WebResourceFactory.authenticated();
        anonymousWebResource = WebResourceFactory.anonymous();
    }

    protected void assertResponseContainsAntiSniffHeader(ClientResponse response) {
        assertEquals("nosniff", response.getHeaders().getFirst(
                AntiSniffingResponseFilter.ANTI_SNIFFING_HEADER_NAME));
    }

    @Test
    public void testAnonymousRestResponseContainsAntiSniffingHeader() {
        assertResponseContainsAntiSniffHeader(anonymousWebResource.path(
                "projects").get(ClientResponse.class));
    }

    @Test
    public void testAuthenticatedRestResponseContainsAntiSniffingHeader() {
        assertResponseContainsAntiSniffHeader(authenticatedWebResource.path(
                "projects").get(ClientResponse.class));
    }

    @Test
    public void testErrorPageRestResponseContainsAntiSniffingHeader() {
        ClientResponse clientResponse = anonymousWebResource.path(
                "somepaththatdoesntexist").get(ClientResponse.class);
        assertResponseContainsAntiSniffHeader(clientResponse);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
                clientResponse.getStatus());
    }
}
