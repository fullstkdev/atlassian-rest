package it.com.atlassian.rest.mvc;

import com.atlassian.plugins.rest.mvc.Velocity;
import com.atlassian.rest.jersey.client.WebResourceFactory;

import static org.junit.Assert.*;

import org.junit.Test;

public class VelocityTest {
    @Test
    public void testGetVelocityResource() {
        final String text = WebResourceFactory.anonymous().path("velocity").get(String.class);
        assertEquals(Velocity.TITLE + "\n" + Velocity.SUB_TITLE, text);
    }
}
