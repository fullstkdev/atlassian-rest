package com.atlassian.plugins.rest.sample.entities;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import com.sun.jersey.spi.resource.Singleton;

@Path("fruit-basket")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
public class EntityListResource {
    @POST
    @AnonymousAllowed
    public Response echo(final List<Orange> basket, @Context HttpHeaders headers) {
        return Response.ok(basket.get(0)).build();
    }
}
