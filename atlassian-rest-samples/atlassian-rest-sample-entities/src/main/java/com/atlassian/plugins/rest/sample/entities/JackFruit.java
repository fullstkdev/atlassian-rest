package com.atlassian.plugins.rest.sample.entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A fruit with a mix of JAXB and Jackson annotations. The Jackson annotations
 * should take precedence when this resource is requested as JSON.
 */
@XmlRootElement
public class JackFruit {
    @JsonProperty("json-description")
    @XmlAttribute(name = "jaxb-description")
    public final String description;

    JackFruit(String description) {
        this.description = description;
    }

    public JackFruit() {
        this("through Jersey");
    }
}
