package com.atlassian.plugins.rest.sample.entities;

import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class UriBuilder {
    private static final String CONTEXT_SYSTEM_PROPERTY = "context.path";
    private static final String HTTP_PORT_SYSTEM_PROPERTY = "http.port";
    private static final int DEFAULT_HTTP_PORT = 0;

    private static final String CONTEXT;
    private static final int HTTP_PORT;

    static {
        CONTEXT = System.getProperty(CONTEXT_SYSTEM_PROPERTY);
        HTTP_PORT = Integer.valueOf(System.getProperty(HTTP_PORT_SYSTEM_PROPERTY, String.valueOf(DEFAULT_HTTP_PORT)));

        if (CONTEXT == null || HTTP_PORT == DEFAULT_HTTP_PORT) {
            throw new IllegalStateException(CONTEXT_SYSTEM_PROPERTY + " or " + HTTP_PORT_SYSTEM_PROPERTY + " not set!");
        }
    }

    private final List<String> paths = new LinkedList<String>();

    private UriBuilder() {
    }

    public static UriBuilder create() {
        return new UriBuilder();
    }

    public UriBuilder path(String path) {
        paths.add(path);
        return this;
    }

    public URI build() {
        try {
            return new URI("http", null, "localhost", HTTP_PORT, buildPath(), buildQuery(), buildFragment());
        } catch (URISyntaxException e) {
            // will not happen
            return null;
        }
    }

    private String buildPath() {
        final StringBuilder path = new StringBuilder(paths.size() * 8);
        if (StringUtils.isNotBlank(CONTEXT)) {
            path.append(CONTEXT).append("/");
        }
        for (String element : paths) {
            path.append(element).append("/");
        }
        if (path.charAt(0) != '/') {
            path.insert(0, '/');
        }
        return path.toString();
    }

    private String buildQuery() {
        return null;
    }

    private String buildFragment() {
        return null;
    }
}
